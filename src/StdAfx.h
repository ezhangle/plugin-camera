/* Camera_Plugin - for licensing and copyright see license.txt */

#pragma once

// ***
// *** These definitions are copied from the stdafx.h from the stock GameSDK.
// ***

#include <CryModuleDefs.h>
#define eCryModule eCryM_Game
#define RWI_NAME_TAG "RayWorldIntersection(Game)"
#define PWI_NAME_TAG "PrimitiveWorldIntersection(Game)"

#include <platform.h>
#include <algorithm>
#include <vector>
#include <memory>
#include <list>
#include <map>
#include <functional>
#include <limits>
#include <smartptr.h>
#include <VectorSet.h>
#include <StlUtils.h>
#include <CryThread.h>
#include <Cry_Math.h>
#include <Cry_Camera.h>
#include <CryListenerSet.h>
#include <ISystem.h>
#include <I3DEngine.h>
#include <IParticles.h>
#include <IInput.h>
#include <IConsole.h>
#include <ITimer.h>
#include <ILog.h>
#include <IGameplayRecorder.h>
#include <ISerialize.h>
#include "CryMacros.h"
#include <GameUtils.h>
#include "CheatProtection.h"

#ifndef GAMEDLL_EXPORTS
#define GAMEDLL_EXPORTS
#endif

#ifdef GAMEDLL_EXPORTS
#define GAME_API DLL_EXPORT
#else
#define GAME_API
#endif

# ifdef _DEBUG
#  ifndef DEBUG
#    define DEBUG
#  endif
#endif

#if defined(XENON) || defined(PS3)
#define MAX_PLAYER_LIMIT 12
#else
#define MAX_PLAYER_LIMIT 16
#endif

#include "Game.h"


// ***
// *** From here, the definitions are specific to the needs of the Plugin system.
// ***

#ifndef _FORCEDLL
#define _FORCEDLL
#endif

#ifndef CAMERAPLUGIN_EXPORTS
#define CAMERAPLUGIN_EXPORTS
#endif

#pragma warning(disable: 4018)  // conditional expression is constant

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#pragma once

//#include <ICameraMode.h>
#include <Chrysalis/Player/Camera/CameraHandler.h>


class CCameraPose;


namespace Chrysalis
{
	struct ICamera : public IPlayerCameraListener
	{
		struct AnimationSettings
		{
			AnimationSettings () : positionFactor (0.0f), rotationFactor (0.0f), applyResult (false), stableBlendOff (false)
			{
			}

			float positionFactor;
			float rotationFactor;
			bool  applyResult;
			bool  stableBlendOff;
		};

		ICamera ();
		virtual ~ICamera ();


		/**
		The camera is allowed to respond to update events.
		 */
		void EnableUpdates () { m_isEnabled = true; };


		/**
		The camera is not allowed to respond to update events.
		 */
		void DisableUpdates () { m_isEnabled = false; };


		/**
		Query if the camera can respond to update events.
		
		\return	true if it will call it's update function, false if not.
		 */
		bool IsUpdateEnabled (void) const { return(m_isEnabled); };


		/**
		Activates this camera.
		
		\param	clientPlayer	The client player.
		 */
		virtual void Activate (const CPlayer& clientPlayer) = 0;


		/**
		Deactivates this camera.
		
		\param	clientPlayer	The client player.
		 */
		virtual void Deactivate (const CPlayer& clientPlayer) = 0;


		/**
		Activates this camera.
		
		\param	clientPlayer	The client player.
		 */
		void ActivateMode (const CPlayer & clientPlayer);


		/**
		Deactivates this camera.
		
		\param	clientPlayer	The client player.
		 */
		void DeactivateMode (const CPlayer & clientPlayer);


		/**
		Query if this camera allows debugging.
		
		\return	true if debug allowed, false if not.
		 */
		bool IsDebugEnabled (void) const { return(m_isDebugAllowed); };


		/**
		Enables debugging helpers.
		 */
		void EnableDebug () { m_isDebugAllowed = true; };


		/**
		Disables debugging helpers.
		 */
		void DisableDebug () { m_isDebugAllowed = false; };


		/**
		Performs all the calculations required to position the camera for the present frame.
		
		\param	clientPlayer	The client player.
		\param	viewParams  	Options for controlling the view.
		\param	frameTime   	The length of this frame expressed as a fraction of a second.
		
		\return	A CCameraPose indicating the desired camera placement.
		 */
		virtual CCameraPose UpdateView (const CPlayer& clientPlayer, SViewParams& viewParams, float frameTime) = 0;


		/**
		Is this camera capable of transitioning?
		
		\return	true if we can transition, false if not.
		 */
		virtual bool CanTransition ();


		/**
		Sets camera animation factor.
		
		\param	animationSettings	The animation settings.
		 */
		virtual void SetCameraAnimationFactor (const AnimationSettings& animationSettings);


		/**
		Gets camera animation factor.
		
		\param [in,out]	position	The position.
		\param [in,out]	rotation	The rot.
		 */
		virtual void GetCameraAnimationFactor (float& position, float& rotation);


		/**
		Query if the ability to blend between frames is off.
		
		\return	true if blending off, false if not.
		 */
		const bool IsBlendingOff () const
		{
			return m_isBlendingOff;
		}

	protected:

		/**
		Applies the pitch and yaw delta from the XBox controller. This needs to be called every
		update if we wish to support input from an XBox controller.
		
		\param	frameTime	The frame time.
		 */
		virtual void ApplyXIPitchYaw (float frameTime) = 0;


		/**
		Resets the results of all player based camera movements back to their defaults.
		 */
		virtual void ResetMovements () { CCameraHandler::ResetMovements (); };


		/**
		TODO: document this.
		
		\param	clientPlayer   	The client player.
		\param	drawNearestFlag	true to draw nearest flag.
		 */
		void SetDrawNearestFlag (const CPlayer & clientPlayer, bool drawNearestFlag);


		/**
		Is the camera enabled?
		 */
		bool m_isEnabled;


		/**
		Is debugging allowed?
		 */
		bool m_isDebugAllowed;


		/**
		Is blending switched off?
		 */
		bool m_isBlendingOff;


		/**
		TODO: document this.
		 */
		bool m_disableDrawNearest;
	};
}
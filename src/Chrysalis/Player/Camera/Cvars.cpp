#include <StdAfx.h>
#include <Chrysalis/Player/Camera/Cvars.h>


namespace Chrysalis
{
	// Determine the level of zoom on the third person camera.
	float cl_tpvZoom = 0.5f;
	float cl_tpvZoomMin = 0.1f;
	float cl_tpvZoomMax = 1.0f;
	float cl_tpvZoomStep = 0.05f;
	float cl_tpvZoomSpeed = 20.0f;

	// Pitch and yaw for the camera from mouse movements.
	int   cl_tpvMouseInvertPitch = 0;
	float cl_tpvViewPitch = 0.0;
	float cl_tpvViewYaw = 0.0;

	// Pitch and yaw from the XBox controller.
	float cl_tpvXIPitchDelta = 0.0;
	float cl_tpvXIYawDelta = 0.0;

	// Minimum and maximum pitch / yaw rotations in degrees.
	float cl_tpvPitchMin = -90.0;
	float cl_tpvPitchMax = 90.0;
	float cl_tpvYawMin = 0.0;
	float cl_tpvYawMax = 360.0;

	// Filter pitch and yaw adjustments below this threshold (radians).
	float cl_tpvPitchFilter = 0.015f;
	float cl_tpvYawFilter = 0.015f;

	// Mouse sensitivity for pitching and yawing.
	float cl_tpvPitchYawSensitivity = 8.0f;

	// Distance the camera is panned from the present locus in world co-ordinates.
	float cl_tpvPanHorizontal = 0.0f;
	float cl_tpvPanVertical = 0.0f;

	// Mouse sensitivity for panning.
	float cl_tpvPanMouseSensitivity = 1.0f;

	// Speed in metres / sec to pan when using button / key panning.
	float cl_tpvPanKeySpeed = 8.0f;

	// Maximum and minimum distances the camera is allowed to pan from the locus.
	float cl_tpvPanHorizontalOffsetMax = 4096.0f;
	float cl_tpvPanHorizontalOffsetMin = -4096.0f;
	float cl_tpvPanVerticalOffsetMax = 4096.0f;
	float cl_tpvPanVerticalOffsetMin = -4096.0f;

	// Map co-ordinate constraints. The camera may only pan around within this bounding box.
	float cl_tpvPanHorizontalMax = 4096.0f;
	float cl_tpvPanHorizontalMin = 0.0f;
	float cl_tpvPanVerticalMax = 4096.0f;
	float cl_tpvPanVerticalMin = 0.0f;

	// Mouse look
	int cl_tpvMouseLook = 0;

	// Freelook
	int cl_tpvFreelook = true;

	// Camera panning using button.
	int ButtonPanningMask = 0;

		
	void RegisterCameraCvars ()
	{
		if (gEnv && gEnv->pConsole)
		{
			REGISTER_CVAR (cl_tpvZoom, 0.5f, 0, "3rd person camera - zoom level (0.0 - 1.0)");
			REGISTER_CVAR (cl_tpvZoomMin, 0.0f, 0, "3rd person camera - minimum amount we allow the camera to zoom");
			REGISTER_CVAR (cl_tpvZoomMax, 1.0f, 0, "3rd person camera - maximum amount we allow the camera to zoom");
			REGISTER_CVAR (cl_tpvZoomStep, 0.05f, 0, "3rd person camera - zoom step increments");
			REGISTER_CVAR (cl_tpvZoomSpeed, 20.0f, 0, "3rd person camera - interpolation speed for zoom movements.");

			REGISTER_CVAR (cl_tpvMouseInvertPitch, 0.0f, 0, "3rd person camera - invert Y axis");
			REGISTER_CVAR (cl_tpvViewPitch, 0.0f, 0, "3rd person camera - player mouse controlled pitch");
			REGISTER_CVAR (cl_tpvViewYaw, 0.0f, 0, "3rd person camera - player mouse controlled yaw");
			REGISTER_CVAR (cl_tpvXIPitchDelta, 0.0f, 0, "3rd person camera - player Xbox controller pitch");
			REGISTER_CVAR (cl_tpvXIYawDelta, 0.0f, 0, "3rd person camera - player Xbox controller yaw");
			REGISTER_CVAR (cl_tpvPitchFilter, 0.015f, 0, "3rd person camera - pitch filter");
			REGISTER_CVAR (cl_tpvYawFilter, 0.015f, 0, "3rd person camera - yaw filter");
			REGISTER_CVAR (cl_tpvPitchYawSensitivity, 8.0f, 0, "3rd person camera - pitch / yaw mouse sensitivity");

			REGISTER_CVAR (cl_tpvPanHorizontal, 0.0f, 0, "3rd person camera - pan - horizontal amount");
			REGISTER_CVAR (cl_tpvPanVertical, 0.0f, 0, "3rd person camera - pan - vertical amount");

			REGISTER_CVAR (cl_tpvPanMouseSensitivity, 1.0f, 0, "3rd person camera - pan - sensitivity of the mouse for panning movements");
			REGISTER_CVAR (cl_tpvPanKeySpeed, 8.0f, 0, "3rd person camera - m/s to pan camera when activated by the keyboard");

			REGISTER_CVAR (cl_tpvPanHorizontalOffsetMax, 4096.0f, 0, "3rd person camera - pan - limit on horizontal movement from the locus");
			REGISTER_CVAR (cl_tpvPanHorizontalOffsetMin, -4096.0f, 0, "3rd person camera - pan - limit on horizontal movement from the locus");
			REGISTER_CVAR (cl_tpvPanVerticalOffsetMax, 4096.0f, 0, "3rd person camera - pan - limit on vertical movement from the locus");
			REGISTER_CVAR (cl_tpvPanVerticalOffsetMin, -4096.0f, 0, "3rd person camera - pan - limit on vertical movement from the locus");

			REGISTER_CVAR (cl_tpvPanHorizontalMax, 4096.0f, 0, "3rd person camera - pan - horizontal map co-ordinate constraints.");
			REGISTER_CVAR (cl_tpvPanHorizontalMin, 0.0f, 0, "3rd person camera - pan - horizontal map co-ordinate constraints.");
			REGISTER_CVAR (cl_tpvPanVerticalMax, 4096.0f, 0, "3rd person camera - pan - vertical map co-ordinate constraints.");
			REGISTER_CVAR (cl_tpvPanVerticalMin, 0.0f, 0, "3rd person camera - pan - vertical map co-ordinate constraints.");

			REGISTER_CVAR (cl_tpvMouseLook, 0, 0, "Mouse movements pan / tilt / rotate the view");
			REGISTER_CVAR (cl_tpvFreelook, 1, 0, "Allow freelook?");
		}
	}


	void UnregisterCameraCvars ()
	{
		if (gEnv && gEnv->pConsole)
		{
			gEnv->pConsole->UnregisterVariable ("cl_tpvZoom", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvZoomMin", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvZoomMax", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvZoomStep", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvZoomSpeed", true);

			gEnv->pConsole->UnregisterVariable ("cl_tpvMouseInvertPitch", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvViewPitch", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvViewYaw", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvXIPitchDelta", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvXIYawDelta", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvPitchFilter", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvYawFilter", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvPitchFilter", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvPitchYawSensitivity", true);

			gEnv->pConsole->UnregisterVariable ("cl_tpvPanVertical", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvPanHorizontal", true);

			gEnv->pConsole->UnregisterVariable ("cl_tpvPanMouseSensitivity", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvPanKeySpeed", true);

			gEnv->pConsole->UnregisterVariable ("cl_tpvPanHorizontalOffsetMax", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvPanHorizontalOffsetMin", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvPanVerticalOffsetMax", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvPanVerticalOffsetMin", true);

			gEnv->pConsole->UnregisterVariable ("cl_tpvPanHorizontalMax", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvPanHorizontalMin", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvPanVerticalMax", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvPanVerticalMin", true);

			gEnv->pConsole->UnregisterVariable ("cl_tpvMouseLook", true);
			gEnv->pConsole->UnregisterVariable ("cl_tpvFreelook", true);
		}
	}
}
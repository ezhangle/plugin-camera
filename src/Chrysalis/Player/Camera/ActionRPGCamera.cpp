#include <StdAfx.h>
#include <ISystem.h>
#include <Player.h>
#include <PlayerCamera.h>
#include <CameraModes.h>
#include <Chrysalis/Player/Camera/CameraHandler.h>
#include <Chrysalis/Player/Camera/ActionRPGCamera.h>
#include <Chrysalis/Player/Camera/Cvars.h>


namespace Chrysalis
{
	ActionRPGCamera::ActionRPGCamera () :
		m_pEntity (nullptr),
		m_targetBoneName (""),
		m_targetAimBoneName (""),
		m_viewPositionOffset (Vec3 (ZERO)),
		m_aimPositionOffset (Vec3 (ZERO)),
		m_targetDistance (5.0f),
		m_reversePitchTilt (0.75f),
		m_vecLastPosition (Vec3 (ZERO)),
		m_quatLastTargetRotation (Quat (IDENTITY)),
		m_quatTargetRotation (Quat (IDENTITY)),
		m_vecLastTargetPosition (Vec3 (ZERO)),
		m_vecLastTargetAimPosition (Vec3 (ZERO)),
		m_quatLastViewRotation (Quat (IDENTITY)),
		m_slerpSpeed (20.0f),
		m_skipInterpolation (true)
	{
	}


	void ActionRPGCamera::EnableUpdates ()
	{
		m_isEnabled = true;
	}


	void ActionRPGCamera::DisableUpdates ()
	{
		m_isEnabled = false;
	}


	void ActionRPGCamera::Activate (const CPlayer& clientPlayer)
	{
		EnableUpdates ();
		CCameraHandler::ResetMovements ();
		m_skipInterpolation = true;

		m_pEntity = clientPlayer.GetEntity ();

		// Give the camera an initial orientation based on the player's rotation.
		m_quatTargetRotation = Quat::CreateRotationVDir (m_pEntity->GetForwardDir ());
		cl_tpvViewPitch = 15;

		// Listen for PlayerCamera events.
		clientPlayer.GetPlayerCamera ()->RegisterCameraListener (this);

		// Throw them into third person if they aren't already.
		// TODO: this is a bit naughty, refactor it so CPlayerCamera handles the changeover.
		if (!clientPlayer.IsThirdPerson ())
			const_cast <CPlayer&> (clientPlayer).ToggleThirdPerson ();
	}


	void ActionRPGCamera::Deactivate (const CPlayer& clientPlayer)
	{
		DisableUpdates ();

		// Stop listening for PlayerCamera events.
		clientPlayer.GetPlayerCamera ()->UnregisterCameraListener (this);
	}


	void ActionRPGCamera::GetTargetPositions (IEntity* const pEntity,
		string targetBone, string targetAimBone,
		Vec3& vecTargetPosition, Vec3& vecTargetAimPosition)
	{
		if (pEntity)
		{
			// Default is simply to use the entity position.
			vecTargetAimPosition = vecTargetPosition = pEntity->GetPos ();

			// If they want to target a bone, we need to have an actor to work with.
			if ((targetBone.length () > 0) || (targetAimBone.length () > 0))
			{
				IActor* pActor = gEnv->pGame->GetIGameFramework ()->GetIActorSystem ()->GetActor (pEntity->GetId ());
				if (pActor)
				{
					// Get Animated Character
					IAnimatedCharacter* pAnimCharacter = pActor->GetAnimatedCharacter ();
					ICharacterInstance* pCharacter = pEntity->GetCharacter (0);

					// Get Skeleton Pose
					if (pAnimCharacter && pCharacter)
					{
						ISkeletonPose* pPose = pCharacter->GetISkeletonPose ();
						IDefaultSkeleton& targetSkeleton = pCharacter->GetIDefaultSkeleton ();

						if (pPose)
						{
							// The world position which acts as our camera target.
							int16 targetPositionBoneId = targetSkeleton.GetJointIDByName (targetBone);
							if (targetPositionBoneId >= 0)
								vecTargetPosition = (pAnimCharacter->GetAnimLocation () * pPose->GetAbsJointByID (targetPositionBoneId)).t;

							// The world position of the bone we wish to aim towards.
							int16 targetViewAimBoneId = targetSkeleton.GetJointIDByName (targetAimBone);
							if (targetViewAimBoneId >= 0)
								vecTargetAimPosition = (pAnimCharacter->GetAnimLocation () * pPose->GetAbsJointByID (targetViewAimBoneId)).t;
						}
					}
				}
			}
		}
	}


	void ActionRPGCamera::AttachToEntity (IEntity* const pEntity)
	{
		CRY_ASSERT (pEntity);

		// Attach the camera to the entity.
		m_pEntity = pEntity;
		m_skipInterpolation = true;

		// Give the camera an initial orientation based on the entity's rotation.
		if (pEntity)
			m_quatTargetRotation = Quat::CreateRotationVDir (pEntity->GetForwardDir ());
		else
			m_quatTargetRotation = Quat::CreateIdentity ();
		m_quatLastTargetRotation = m_quatTargetRotation;

		EntityId entityId = pEntity->GetId ();
	}


	bool ActionRPGCamera::CollisionDetection (const Vec3& Goal, Vec3& CameraPosition)
	{
		bool updatedCameraPosition = false;

		// Try and update where the player is aiming.
		// TODO: need to skip the player's geometry and if they are in a vehicle, that needs skipping too.
		ray_hit rayhit;
		static IPhysicalEntity* pSkipEnts [10];
		int nskip = 0;

		int hits = gEnv->pPhysicalWorld->RayWorldIntersection (Goal,
			CameraPosition - Goal,
			ent_all,
			rwi_stop_at_pierceable | rwi_colltype_any,
			&rayhit,
			1, pSkipEnts, nskip + 1);

		if (hits)
		{
			CameraPosition = rayhit.pt;
			updatedCameraPosition = true;
		}

		return updatedCameraPosition;
	}


	void ActionRPGCamera::ApplyXIPitchYaw (float frameTime)
	{
		// Pitch.
		cl_tpvViewPitch += cl_tpvXIPitchDelta;
		cl_tpvViewPitch = clamp_tpl (cl_tpvViewPitch, cl_tpvPitchMin, cl_tpvPitchMax);

		// Yaw.
		cl_tpvViewYaw += cl_tpvXIYawDelta;

		// We want the values to circle around from 0.0 - 360.0
		if (cl_tpvViewYaw > 360.0f)
			cl_tpvViewYaw -= 360.0f;
		if (cl_tpvViewYaw < -0.0f)
			cl_tpvViewYaw += 360.0f;

		// Limit the yaw movement according to the set max and min values.
		cl_tpvViewYaw = clamp_tpl (cl_tpvViewYaw, cl_tpvYawMin, cl_tpvYawMax);
	}


	CCameraPose ActionRPGCamera::UpdateView (const CPlayer& clientPlayer, SViewParams& viewParams, float frameTime)
	{
		if (gEnv && gEnv->pGame && gEnv->pGame->GetIGameFramework () && m_isEnabled && !gEnv->IsCutscenePlaying ())
		{
			// Make sure we have an actor and are in third person mode.
			IActor* pClient = gEnv->pGame->GetIGameFramework ()->GetClientActor ();
			if (pClient && !pClient->IsThirdPerson ())
				return CCameraPose (viewParams.position, viewParams.rotation);

			// XBox controller yaw / pitch deltas need processing.
			ApplyXIPitchYaw (frameTime);

			// Calculate pitch and yaw movements to apply both prior to and after positioning the camera.
			Quat quatPreTransYP = Quat (Ang3 (DEG2RAD (cl_tpvViewPitch), 0.0f, DEG2RAD (cl_tpvViewYaw)));
			Quat quatPostTransYP = Quat::CreateRotationXYZ (Ang3 (DEG2RAD (cl_tpvViewPitch * m_reversePitchTilt), 0.0f, 0.0f));

			// Target and aim position come from the entity position.
			Vec3 vecTargetInitialPosition;
			Vec3 vecTargetInitialAimPosition;
			GetTargetPositions (m_pEntity, m_targetBoneName, m_targetAimBoneName, vecTargetInitialPosition, vecTargetInitialAimPosition);

			// The distance from the view to the target.
			float shapedZoom = (cl_tpvZoom * cl_tpvZoom) / (cl_tpvZoomMax * cl_tpvZoomMax);
			float zoomDistance = m_targetDistance * shapedZoom;

			// Calculate the target rotation and slerp it if we can.
			Quat quatTargetRotationGoal = m_quatTargetRotation * quatPreTransYP;
			Quat quatTargetRotation;
			if (m_skipInterpolation)
			{
				m_quatLastTargetRotation = quatTargetRotation = quatTargetRotationGoal;
			}
			else
			{
				quatTargetRotation = Quat::CreateSlerp (m_quatLastTargetRotation, quatTargetRotationGoal, frameTime * m_slerpSpeed);
				m_quatLastTargetRotation = quatTargetRotation;
			}

			// Work out where to place the new initial position. We will be using a unit vector facing forward Y
			// as the starting place and applying rotations from the target bone and player camera movements.
			Vec3 vecViewPosition = vecTargetInitialPosition + (quatTargetRotation * (Vec3 (0.0f, 1.0f, 0.0f) * zoomDistance))
				+ quatTargetRotation * m_viewPositionOffset;

			// By default, we try and aim the camera at the target, taking into account
			// the current mouse yaw and pitch values. If, by chance, the target and the view position
			// are the same, then we cheat and use the initial target rotation - which should provide an
			// acceptable result, hopefully. Avoid this by not allowing to zoom too close to the target.
			Vec3 vecAimAtTarget = (vecTargetInitialAimPosition - vecViewPosition).GetNormalized ();
			Quat quatViewRotationGoal = vecAimAtTarget.IsZero () ? quatTargetRotation : Quat::CreateRotationVDir (vecAimAtTarget);

			// Use a slerp to smooth out fast camera rotations.
			Quat quatViewRotation;
			if (m_skipInterpolation)
			{
				m_quatLastViewRotation = quatViewRotation = quatViewRotationGoal;
				m_skipInterpolation = false;
			}
			else
			{
				quatViewRotation = Quat::CreateSlerp (m_quatLastViewRotation, quatViewRotationGoal, frameTime * m_slerpSpeed);
				m_quatLastViewRotation = quatViewRotation;
			}

			// Apply a final translation to both the view position and the aim position.
			vecViewPosition += quatViewRotation * m_aimPositionOffset;

			// Gimbal style rotation after it's moved into it's initial position.
			Quat quatOrbitRotation = quatViewRotation * quatPostTransYP;
			Vec3 vecTargetAimPosition = vecTargetInitialAimPosition + quatViewRotation * m_aimPositionOffset;

			// Perform a collision detection. Note, any collisions will disallow use of interpolated
			// camera movement.
			//CollisionDetection (vecTargetAimPosition, vecViewPosition);

#ifdef _DEBUG
			if (m_isDebugAllowed)
			{
				// Initial position for our target.
				gEnv->pRenderer->GetIRenderAuxGeom ()->DrawSphere (vecTargetInitialPosition, 0.16f, ColorB (0, 0, 128));
				gEnv->pRenderer->GetIRenderAuxGeom ()->DrawSphere (vecTargetInitialPosition + quatTargetRotation * m_viewPositionOffset, 0.06f, ColorB (0, 128, 0));
				
				// Initial aim position.
				gEnv->pRenderer->GetIRenderAuxGeom ()->DrawSphere (vecTargetInitialAimPosition, 0.08f, ColorB (128, 0, 0));
			}
#endif

			// Return our new calculated camera pose.
			m_vecLastPosition = vecViewPosition;
			return CCameraPose (vecViewPosition, quatOrbitRotation);
		}

		// Worst case! We return a view based on the view params that were passed in.
		return CCameraPose (viewParams.position, viewParams.rotation);
	}


	void ActionRPGCamera::ResetMovements ()
	{
		ICamera::ResetMovements ();
	}
}


#pragma once

#include <Cry_Math.h>
#include <Tarray.h>


namespace Chrysalis
{
	/**
	A camera pose provides both a position and rotation which may be used to represent the
	position of a camera within the game world. Simple member functions provides helpers for
	composing, scaling and constructing these representations.
	 */
	struct CCameraPose
	{
	public:

		/**
		Constructs a camera with zero position and an identity quaternion for it's rotation component.
		 */
		CCameraPose () :
			m_position (ZERO),
			m_rotation (IDENTITY) {}


		/**
		Constructor.
		
		\param	position	The position.
		\param	rotation	The rotation.
		 */
		CCameraPose (Vec3 position, Quat rotation) :
			m_position (position),
			m_rotation (rotation) {}


		/**
		A constructor which copies it's position and rotation values from another CCameraPose object.
		
		\param	copy	The object from which we will copy the position and rotation members.
		 */
		CCameraPose (const CCameraPose& copy) :
			m_position (copy.m_position),
			m_rotation (copy.m_rotation) {}


		/**
		The left hand camera pose is composed with the right hand camera pose.
		
		\param	lhs	The left hand side.
		\param	rhs	The right hand side.
		
		\return	A CCameraPose that is the result of composing the lhs and the rhs.
		 */
		static CCameraPose Compose (const CCameraPose& lhs, const CCameraPose& rhs)
		{
			return CCameraPose (
				lhs.GetPosition () + rhs.GetPosition (),
				lhs.GetRotation () * rhs.GetRotation ());
		}


		/**
		Scales both the vector and the rotation for the camera pose by the factor provided. The
		rotation is scaled using a SLERP between the identity quaternion and the lhs rotation.
		
		\param	lhs   	The left hand side.
		\param	factor	The factor.
		
		\return	A CCameraPose that is a copy of the lhs scaled by factor.
		 */
		static CCameraPose Scale (const CCameraPose& lhs, float factor)
		{
			if (factor == 1.0f)
				return lhs;
			else
				return CCameraPose (
				lhs.GetPosition () * factor,
				Quat::CreateSlerp (IDENTITY, lhs.GetRotation (), factor));
		}


		/**
		Linear interpolation between two camera poses.
		
		\param	from  	A valid camera pose that defines a position and rotation from which you wish to
						interpolate.
		\param	to	  	A valid camera pose that defines a position and rotation towards which you wish
						to interpolate.
		\param	stride	The stride.
		
		\return	A camera pose that represents a linear interpolation between the 'from' and 'to'
				parameters.
		 */
		static CCameraPose Lerp (const CCameraPose& from, const CCameraPose& to, float stride)
		{
			return CCameraPose (
				LERP (from.GetPosition (), to.GetPosition (), stride),
				Quat::CreateSlerp (from.GetRotation (), to.GetRotation (), stride));
		}


		/**
		Gets the position.
		
		\return	The position.
		 */
		Vec3 GetPosition () const { return m_position; }


		/**
		Gets the rotation.
		
		\return	The rotation.
		 */
		Quat GetRotation () const { return m_rotation; }

	private:
		Vec3 m_position;	///< Position of the camera.
		Quat m_rotation;	///< Rotation of the camera.
	};
}
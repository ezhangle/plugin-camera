#include <StdAfx.h>

#include <Player.h>
#include <CPluginCamera.h>
#include <Chrysalis/Player/Camera/CameraHandler.h>
#include <Chrysalis/Player/Camera/RTSCamera.h>
#include <Chrysalis/Player/Camera/Cvars.h>


namespace Chrysalis
{
	class CRTSCameraNode :
		public CFlowBaseNode<eNCT_Instanced>
	{
	private:

		enum EInputPorts
		{
			// Enable / disable updates.
			EIP_ENABLE,
			EIP_DISABLE,

			// The view position is offset from the target by a given distance in a selected direction.
			EIP_TARGET_DISTANCE,

			// Offset the preferred camera position after the basic calculations are done.
			EIP_VIEW_POSITION_OFFSET,
			EIP_AIM_POSITION_OFFSET,

			// Reverse pitch tilt.
			EIP_REVERSE_PITCH_TILT,

			// Move to the present entity.
			EIP_MOVE_TO_ENTITY,

			// Attach to the present entity.
			EIP_ATTACH_TO_ENTITY,

			// Entity bone.
			EIP_TARGET_BONE,
			EIP_TARGET_AIM_BONE,

			// Move to a given position.
			EIP_MOVE_TO_POSITION,
			EIP_NEW_POSITION,
			EIP_CAMERA_HEIGHT,
			EIP_GROUND_HUGGING_SPEED,

			// Mouse sensitivity for panning.
			EIP_PAN_SENSITIVITY,

			// Speed in metres / sec to pan when using button / key panning.
			EIP_PAN_KEYSPEED,

			// Map co-ordinate constraints. The camera may only pan around within this bounding box.
			EIP_PAN_HORIZONTAL_MAX,
			EIP_PAN_HORIZONTAL_MIN,
			EIP_PAN_VERTICAL_MAX,
			EIP_PAN_VERTICAL_MIN,

			// Maximum distance to allow panning before leashing the camera.
			EIP_PAN_LEASH,

			// The speed at which we apply spherical linear interpolation to the camera rotations.
			EIP_SLERP_SPEED,

			// Debug.
			EIP_DEBUG,
		};

		RTSCamera* m_pCamera;

		// Track the entity for this FG node.
		IEntity* m_pEntity;

	public:

		CRTSCameraNode (SActivationInfo* pActInfo) :
			m_pEntity (nullptr)
		{
			m_pCamera = new RTSCamera ();
		}


		virtual ~CRTSCameraNode ()
		{
			delete m_pCamera;
		}


		virtual void GetMemoryUsage (ICrySizer* s) const
		{
			s->Add (*this);
		}


		virtual IFlowNodePtr Clone (SActivationInfo* pActInfo)
		{
			return new CRTSCameraNode (pActInfo);
		}


		virtual void GetConfiguration (SFlowNodeConfig& config)
		{
			static const SInputPortConfig inputs [] =
			{
				InputPortConfig_Void ("Enable", _HELP ("Enable")),
				InputPortConfig_Void ("Disable", _HELP ("Disable")),

				InputPortConfig<float> ("Distance", 5.0f, _HELP ("Ideal distance from the camera to the target"), "Distance"),

				InputPortConfig<Vec3> ("ViewPositionOffset", Vec3 (ZERO), _HELP ("The camera view position is translated in camera space after inital calculations are made"), "View Position Offset"),
				InputPortConfig<Vec3> ("AimPositionOffset", Vec3 (ZERO), _HELP ("The position the camera is aiming at is translated in camera space after inital calculations are made"), "Aim Position Offset"),

				InputPortConfig<float> ("ReversePitchTilt", 0.75f, _HELP ("Tilts the pitch of the camera away from the aim target by this amount of the current pitch"), "Reverse Pitch Tilt"),

				InputPortConfig_Void ("MoveToEntity", _HELP ("Moves the camera to the present entity. Utilises bones if they are available."), "Move To Entity"),
				InputPortConfig_Void ("AttachToEntity", _HELP ("Attaches the camera to the present entity via a leash. Utilises bones if they are available."), "Attach To Entity"),
				InputPortConfig<string> ("bone_TargetPositionBone", "", _HELP ("This bone provides the starting position for the camera view"), "Target Position Bone", _UICONFIG ("ref_entity=entityId")),
				InputPortConfig<string> ("bone_TargetViewAimBone", "", _HELP ("This is the bone that provides the position the camera view will attempt to aim towards"), "Target View Aim Bone", _UICONFIG ("ref_entity=entityId")),

				InputPortConfig_Void ("MoveToPosition", _HELP ("Moves the camera to a given position."), "Move To Position"),
				InputPortConfig<Vec3> ("NewPosition", Vec3 (ZERO), _HELP ("The camera is moved to the position specified. Z is calculated."), "New Position"),
				InputPortConfig<float> ("CameraHeight", 0.0f, _HELP ("The camera target should float this high above the terrain position."), "Camera Height"),
				InputPortConfig<float> ("GroundHuggingSpeed", 0.1f, _HELP ("Speed at which to interpolate between changes in ground height while panning."), "Ground Hugging Speed"),

				// Mouse sensitivity for panning.
				InputPortConfig<float> ("PanMouseSensitivity", 1.0f, _HELP ("xxx"), "Pan Mouse Sensitivity"),

				// Speed in metres / sec to pan when using button / key panning.
				InputPortConfig<float> ("PanKeySpeed", 8.0f, _HELP ("xxx"), "Pan Key Speed"),

				// Map co-ordinate constraints. The camera may only pan around within this bounding box.
				InputPortConfig<float> ("PanHorizontalMax", 4096.0f, _HELP ("PanHorizontalMax"), "Pan Horizontal Max"),
				InputPortConfig<float> ("PanHorizontalMin", 0.0f, _HELP ("PanHorizontalMin"), "Pan Horizontal Min"),
				InputPortConfig<float> ("PanVerticalMax", 4096.0f, _HELP ("PanVerticalMax"), "Pan Vertical Max"),
				InputPortConfig<float> ("PanVerticalMin", 0.0f, _HELP ("PanVerticalMin"), "Pan Vertical Min"),

				// Maximum distance we can pan from the locus before breaking our leash.
				InputPortConfig<float> ("PanLeash", 5.0f, _HELP ("PanLeash"), "Pan Leash"),

				InputPortConfig<float> ("SlerpSpeed", 20.0f, _HELP ("The speed at which we apply spherical linear interpolation to the camera rotations (1.0f - 100.0f"), "Slerp Speed"),

				// Debugging.
				InputPortConfig<bool> ("Debug", false, _HELP ("Provides debug output, helpers and disables the view"), "Debugging"),

				InputPortConfig_Null (),
			};

			static const SOutputPortConfig outputs [] =
			{
				OutputPortConfig_Null (),
			};

			config.pInputPorts = inputs;
			config.pOutputPorts = outputs;
			config.sDescription = _HELP ("Control third person RTS camera. Not implemented yet! Do not use this.");

			config.nFlags |= EFLN_TARGET_ENTITY;
			config.SetCategory (EFLN_APPROVED);
		}


		virtual void ProcessEvent (EFlowEvent evt, SActivationInfo* pActInfo)
		{
			switch (evt)
			{
				case eFE_Initialize:
					// Sent once after the level has been loaded. It is NOT called on Serialization!
					// This may actually be called several times when using the editor.
					OnInitialize (pActInfo);
					break;

				case eFE_Activate:
				{
					if (IsPortActive (pActInfo, EIP_ATTACH_TO_ENTITY))
					{
						// Attach to the present entity.
						OnAttachToEntity (pActInfo);
					}
					else if (IsPortActive (pActInfo, EIP_MOVE_TO_ENTITY))
					{
						// Move to the present entity.
						OnMoveToEntity (pActInfo);
					}
					else if (IsPortActive (pActInfo, EIP_MOVE_TO_POSITION))
					{
						// Move to a given terrain position.
						OnMoveToPosition (pActInfo);
					}

					// Enable the updates.
					if (IsPortActive (pActInfo, EIP_ENABLE))
						OnEnable (pActInfo);

					// Disable the updates.
					if (IsPortActive (pActInfo, EIP_DISABLE))
						OnDisable (pActInfo);
				}
					break;

				case eFE_SetEntityId:
					m_pEntity = pActInfo->pEntity;
					break;
			}
		}


		void OnInitialize (SActivationInfo* pActInfo)
		{
		}


		void OnEnable (SActivationInfo* pActInfo)
		{
			IActor* pActor = gEnv->pGame->GetIGameFramework ()->GetClientActor ();
			if (pActor && pActor->IsPlayer ())
			{
				CPlayer* pPlayer = static_cast <CPlayer*> (pActor);
				m_pCamera->Activate (*pPlayer);
			}
		}


		void OnDisable (SActivationInfo* pActInfo)
		{
			IActor* pActor = gEnv->pGame->GetIGameFramework ()->GetClientActor ();
			if (pActor && pActor->IsPlayer ())
			{
				CPlayer* pPlayer = static_cast <CPlayer*> (pActor);
				m_pCamera->Deactivate (*pPlayer);
			}
		}


		// Obtain the shared settings which are used when setting a camera position / target entity.

		void CommonSettings (SActivationInfo* pActInfo)
		{
			// Mouse sensitivity for panning.
			cl_tpvPanMouseSensitivity = GetPortFloat (pActInfo, EIP_PAN_SENSITIVITY);

			// Speed in metres / sec to pan when using button / key panning.
			cl_tpvPanKeySpeed = GetPortFloat (pActInfo, EIP_PAN_KEYSPEED);

			// Map co-ordinate constraints. The camera may only pan around within this bounding box.
			cl_tpvPanHorizontalMax = GetPortFloat (pActInfo, EIP_PAN_HORIZONTAL_MAX);
			cl_tpvPanHorizontalMin = GetPortFloat (pActInfo, EIP_PAN_HORIZONTAL_MIN);
			cl_tpvPanVerticalMax = GetPortFloat (pActInfo, EIP_PAN_VERTICAL_MAX);
			cl_tpvPanVerticalMin = GetPortFloat (pActInfo, EIP_PAN_VERTICAL_MIN);

			// Preferred distance in metres to the view camera.
			m_pCamera->SetTargetDistance (GetPortFloat (pActInfo, EIP_TARGET_DISTANCE));

			// Once the camera is in it's initial position, we translate it in camera space by this amount.
			m_pCamera->SetViewPositionOffset (GetPortVec3 (pActInfo, EIP_VIEW_POSITION_OFFSET));
			m_pCamera->SetAimPositionOffset (GetPortVec3 (pActInfo, EIP_AIM_POSITION_OFFSET));

			// Reverse pitch tilt.
			m_pCamera->SetReversePitchTilt (GetPortFloat (pActInfo, EIP_REVERSE_PITCH_TILT));

			// Set the preferred camera height above ground level.
			m_pCamera->SetCameraHeight (GetPortFloat (pActInfo, EIP_CAMERA_HEIGHT));

			// Speed at which to interpolate between changes in ground height while panning.
			m_pCamera->SetGroundHuggingSpeed (GetPortFloat (pActInfo, EIP_GROUND_HUGGING_SPEED));

			// The speed at which we apply spherical linear interpolation to the camera rotations.
			m_pCamera->SetSlerpSpeed (GetPortFloat (pActInfo, EIP_SLERP_SPEED));

			// Debugging.
			GetPortBool (pActInfo, EIP_DEBUG) ? m_pCamera->EnableDebug () : m_pCamera->DisableDebug ();
		}


		// Handles the settings that are common between attaching to an entity and simply
		// jumping to it's location.

		void CommonEntitySettings (SActivationInfo* pActInfo)
		{
			// Make sure we apply settings that are common.
			CommonSettings (pActInfo);

			// Bones within the skeleton, used for positioning and aiming the camera.
			m_pCamera->SetTargetPositionBone (GetPortString (pActInfo, EIP_TARGET_BONE));
			m_pCamera->SetTargetViewAimBone (GetPortString (pActInfo, EIP_TARGET_AIM_BONE));

			// Reset some camera movements.
			cl_tpvPanHorizontal = (cl_tpvPanHorizontalMax + cl_tpvPanHorizontalMin) / 2;
			cl_tpvPanVertical = (cl_tpvPanVerticalMax + cl_tpvPanVerticalMin) / 2;
		}


		void OnAttachToEntity (SActivationInfo* pActInfo)
		{
			// Make sure we apply settings that are common.
			CommonEntitySettings (pActInfo);

			// We attach to the entity, and will follow it around.
			if (m_pEntity)
				m_pCamera->AttachToEntity (m_pEntity);
		}


		// The camera moves to the present location of a given entity. It is left
		// in a detached state.

		void OnMoveToEntity (SActivationInfo* pActInfo)
		{
			// Make sure we apply settings that are common.
			CommonEntitySettings (pActInfo);

			// Move the camera to an entity without attaching to that entity.
			if (m_pEntity)
				m_pCamera->MoveToEntity (m_pEntity);
		}


		// The camera is moved to a location on the map.

		void OnMoveToPosition (SActivationInfo* pActInfo)
		{
			// Make sure we apply settings that are common.
			CommonSettings (pActInfo);

			// Ask the camera to move to the new position.
			m_pCamera->MoveToPosition (GetPortVec3 (pActInfo, EIP_NEW_POSITION));

			// Forget about the last entity we were attached to.
			m_pEntity = nullptr;
		}
	};
}

REGISTER_FLOW_NODE_EX ("Chrysalis:Camera:RTSCamera", Chrysalis::CRTSCameraNode, CRTSCameraNode);
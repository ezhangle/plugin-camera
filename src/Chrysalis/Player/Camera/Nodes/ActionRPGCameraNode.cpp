#include <StdAfx.h>

#include <IRenderAuxGeom.h>
#include <CPluginCamera.h>
#include <Player.h>
#include <PlayerCamera.h>
#include <CameraModes.h>
#include <CryAction.h>
#include <Chrysalis/Player/Camera/CameraHandler.h>
#include <Chrysalis/Player/Camera/ActionRPGCamera.h>


namespace Chrysalis
{
	class CActionRPGCameraNode :
		public CFlowBaseNode<eNCT_Instanced>
	{
	private:

		enum EInputPorts
		{
			// Apply the settings.
			EIP_SET,

			// Enable / disable updates.
			EIP_ENABLE,
			EIP_DISABLE,

			// Entity bone.
			EIP_TARGET_BONE,
			EIP_TARGET_AIM_BONE,

			// The view position is offset from the target by a given distance in a selected direction.
			EIP_TARGET_DISTANCE,

			// Offset the preferred camera position after the basic calculations are done.
			EIP_VIEW_POSITION_OFFSET,
			EIP_AIM_POSITION_OFFSET,

			// Reverse pitch tilt.
			EIP_REVERSE_PITCH_TILT,

			// The speed at which we apply spherical linear interpolation to the camera rotations.
			EIP_SLERP_SPEED,

			// Debug.
			EIP_DEBUG,
		};

		ActionRPGCamera* m_pCamera;

		// Track the entity for this FG node.
		IEntity* m_pEntity;

	public:

		CActionRPGCameraNode (SActivationInfo* pActInfo) :
			m_pEntity (nullptr)
		{
			m_pCamera = new ActionRPGCamera ();
		}


		virtual ~CActionRPGCameraNode ()
		{
			delete m_pCamera;
		}


		virtual void GetMemoryUsage (ICrySizer* s) const
		{
			s->Add (*this);
		}


		virtual IFlowNodePtr Clone (SActivationInfo* pActInfo)
		{
			return new CActionRPGCameraNode (pActInfo);
		}


		virtual void GetConfiguration (SFlowNodeConfig& config)
		{
			static const SInputPortConfig inputs [] =
			{
				InputPortConfig_Void ("Set", _HELP ("Applies all the settings for the ActionRPG camera."), "Set"),

				InputPortConfig_Void ("Enable", _HELP ("Enable")),
				InputPortConfig_Void ("Disable", _HELP ("Disable")),

				InputPortConfig<string> ("bone_TargetPositionBone", "", _HELP ("This bone provides the starting position for the camera view"), "Target Position Bone", _UICONFIG ("ref_entity=entityId")),
				InputPortConfig<string> ("bone_TargetViewAimBone", "", _HELP ("This is the bone that provides the position the camera view will attempt to aim towards"), "Target View Aim Bone", _UICONFIG ("ref_entity=entityId")),

				InputPortConfig<float> ("Distance", 5.0f, _HELP ("Ideal distance from the camera to the target"), "Distance"),

				InputPortConfig<Vec3> ("ViewPositionOffset", Vec3 (ZERO), _HELP ("The camera view position is translated in camera space after inital calculations are made"), "View Position Offset"),
				InputPortConfig<Vec3> ("AimPositionOffset", Vec3 (ZERO), _HELP ("The position the camera is aiming at is translated in camera space after inital calculations are made"), "Aim Position Offset"),

				// Reverse pitch tilt.
				InputPortConfig<float> ("ReversePitchTilt", 0.75f, _HELP ("Tilts the pitch of the camera away from the aim target by this amount of the current pitch"), "Reverse Pitch Tilt"),

				InputPortConfig<float> ("SlerpSpeed", 20.0f, _HELP ("The speed at which we apply spherical linear interpolation to the camera rotations (1.0f - 100.0f"), "Slerp Speed"),

				// Debugging.
				InputPortConfig<bool> ("Debug", false, _HELP ("Provides debug output, helpers and disables the view"), "Debugging"),

				InputPortConfig_Null (),
			};

			static const SOutputPortConfig outputs [] =
			{
				OutputPortConfig_Null (),
			};

			config.pInputPorts = inputs;
			config.pOutputPorts = outputs;
			config.sDescription = _HELP ("Control third person ActionRPG camera. Not implemented yet! Do not use this.");

			config.nFlags |= EFLN_TARGET_ENTITY;
			config.SetCategory (EFLN_APPROVED);
		}


		virtual void ProcessEvent (EFlowEvent evt, SActivationInfo* pActInfo)
		{
			switch (evt)
			{
				case eFE_Initialize:
					// Sent once after the level has been loaded. It is NOT called on Serialization!
					// This may actually be called several times when using the editor.
					OnInitialize (pActInfo);
					break;

				case eFE_Activate:
				{
					// Activate the settings for this flowgraph.
					if (IsPortActive (pActInfo, EIP_SET))
						OnActivate (pActInfo);

					// Enable the updates.
					if (IsPortActive (pActInfo, EIP_ENABLE))
						OnEnable (pActInfo);

					// Disable the updates.
					if (IsPortActive (pActInfo, EIP_DISABLE))
						OnDisable (pActInfo);
				}
					break;

				case eFE_SetEntityId:
					m_pEntity = pActInfo->pEntity;
					break;
			}
		}


		void OnInitialize (SActivationInfo* pActInfo)
		{
		}


		void OnActivate (SActivationInfo* pActInfo)
		{
			// Bones within the skeleton, used for positioning and aiming the camera.
			m_pCamera->SetTargetPositionBone (GetPortString (pActInfo, EIP_TARGET_BONE));
			m_pCamera->SetTargetViewAimBone (GetPortString (pActInfo, EIP_TARGET_AIM_BONE));

			// Preferred distance in metres to the view camera.
			m_pCamera->SetTargetDistance (GetPortFloat (pActInfo, EIP_TARGET_DISTANCE));

			// Once the camera is in it's initial position, we translate it in camera space by this amount.
			m_pCamera->SetViewPositionOffset (GetPortVec3 (pActInfo, EIP_VIEW_POSITION_OFFSET));
			m_pCamera->SetAimPositionOffset (GetPortVec3 (pActInfo, EIP_AIM_POSITION_OFFSET));

			// Reverse pitch tilt.
			m_pCamera->SetReversePitchTilt (GetPortFloat (pActInfo, EIP_REVERSE_PITCH_TILT));

			// The speed at which we apply spherical linear interpolation to the camera rotations.
			m_pCamera->SetSlerpSpeed (GetPortFloat (pActInfo, EIP_SLERP_SPEED));

			// Debugging.
			GetPortBool (pActInfo, EIP_DEBUG) ? m_pCamera->EnableDebug () : m_pCamera->DisableDebug ();
		}


		void OnEnable (SActivationInfo* pActInfo)
		{
			IActor* pActor = gEnv->pGame->GetIGameFramework ()->GetClientActor ();
			if (pActor && pActor->IsPlayer ())
			{
				CPlayer* pPlayer = static_cast <CPlayer*> (pActor);
				m_pCamera->Activate (*pPlayer);
			}
		}


		void OnDisable (SActivationInfo* pActInfo)
		{
			IActor* pActor = gEnv->pGame->GetIGameFramework ()->GetClientActor ();
			if (pActor && pActor->IsPlayer ())
			{
				CPlayer* pPlayer = static_cast <CPlayer*> (pActor);
				m_pCamera->Deactivate (*pPlayer);
			}
		}
	};
}

REGISTER_FLOW_NODE_EX ("Chrysalis:Camera:ActionRPGCamera", Chrysalis::CActionRPGCameraNode, CActionRPGCameraNode);
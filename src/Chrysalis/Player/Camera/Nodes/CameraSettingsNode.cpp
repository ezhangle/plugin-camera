#include <StdAfx.h>

#include <CPluginCamera.h>
#include <Player.h>
#include <PlayerCamera.h>
#include <CameraModes.h>
#include <Chrysalis/Player/Camera/CameraHandler.h>
#include <Chrysalis/Player/Camera/Cvars.h>


namespace Chrysalis
{
	class CCameraSettingsNode :
		public CFlowBaseNode<eNCT_Instanced>
	{
	private:

		enum EInputPorts
		{
			// Apply the settings.
			EIP_SET,

			// Invert the mouse pitch?
			EIP_MOUSE_INVERT_PITCH,

			// View pitch / yaw limitations.
			EIP_VIEW_PITCH_MIN,
			EIP_VIEW_PITCH_MAX,
			EIP_VIEW_YAW_MIN,
			EIP_VIEW_YAW_MAX,

			// Filter pitch and yaw adjustments below this threshhold.
			EIP_PITCH_FILTER,
			EIP_YAW_FILTER,

			// Mouse sensitivity for pitching and yawing.
			EIP_PITCH_YAW_SENSITIVITY,

			// Zoom controls.
			EIP_ZOOM,
			EIP_ZOOM_MIN,
			EIP_ZOOM_MAX,
			EIP_ZOOM_STEP,
			EIP_ZOOM_LERP_SPEED,

			// Freelook.
			EIP_FREELOOK,
		};

		SActivationInfo* m_pActInfo;

	public:

		CCameraSettingsNode (SActivationInfo* pActInfo)
		{
		}


		virtual ~CCameraSettingsNode ()
		{
		}


		virtual void GetMemoryUsage (ICrySizer* s) const
		{
			s->Add (*this);
		}


		virtual IFlowNodePtr Clone (SActivationInfo* pActInfo)
		{
			return new CCameraSettingsNode (pActInfo);
		}


		virtual void GetConfiguration (SFlowNodeConfig& config)
		{
			static const SInputPortConfig inputs [] =
			{
				InputPortConfig_Void ("Set", _HELP ("Applies all the settings to the third person camera."), "Set"),

				InputPortConfig<bool> ("InvertMousePitch", false, _HELP ("Invert the mouse pitch"), "Invert Mouse Pitch"),

				InputPortConfig<float> ("PitchMin", -90, _HELP ("Minimum pitch in degrees"), "Minimum pitch"),
				InputPortConfig<float> ("PitchMax", 90, _HELP ("Maximum pitch in degrees"), "Maximum pitch"),
				InputPortConfig<float> ("YawMin", 0, _HELP ("Minimum yaw in degrees"), "Minimum yaw"),
				InputPortConfig<float> ("YawMax", 360, _HELP ("Maximum yaw in degrees"), "Maximum yaw"),

				InputPortConfig<float> ("PitchFilter", 0.015f, _HELP ("Filter out changes in pitch below this (in degrees)"), "Pitch Filter"),
				InputPortConfig<float> ("YawFilter", 0.015f, _HELP ("Filter out changes in yaw below this (in degrees)"), "Yaw Filter"),

				InputPortConfig<float> ("PitchYawSensitivity", 8.0f, _HELP ("Mouse sensitivity to pitch / yaw movements"), "Pitch / Yaw Sensitivity"),

				InputPortConfig<float> ("Zoom", 0.5f, _HELP ("Current level of zoom"), "Zoom"),
				InputPortConfig<float> ("ZoomMin", 0.1f, _HELP ("Minimum amount the client can zoom in"), "Zoom Minimum"),
				InputPortConfig<float> ("ZoomMax", 1.0f, _HELP ("Maximum amount the client can zoom out"), "Zoom Maximum"),
				InputPortConfig<float> ("ZoomStep", 0.05f, _HELP ("Change in zoom level with each step in / out"), "Zoom Step"),
				InputPortConfig<float> ("ZoomLerpSpeed", 20.0f, _HELP ("Linear interpolation speed for zoom changes"), "Zoom Lerp Speed"),

				InputPortConfig<bool> ("Freelook", 4.0f, _HELP ("Allow freelook movement for the camera pitch and yaw."), "Freelook"),

				InputPortConfig_Null (),
			};

			static const SOutputPortConfig outputs [] =
			{
				OutputPortConfig_Null (),
			};

			config.pInputPorts = inputs;
			config.pOutputPorts = outputs;
			config.sDescription = _HELP ("General purpose third person camera controls.");
			config.SetCategory (EFLN_APPROVED);
		}


		virtual void ProcessEvent (EFlowEvent evt, SActivationInfo* pActInfo)
		{
			switch (evt)
			{
				case eFE_Initialize:
					// Sent once after the level has been loaded. It is NOT called on Serialization!
					// This may actually be called several times when using the editor.
					OnInitialize (pActInfo);
					break;

				case eFE_Activate:

					// Activate the settings for this flowgraph.
					if (IsPortActive (pActInfo, EIP_SET))
						OnActivate (pActInfo);

					break;
			}
		}


		void OnInitialize (SActivationInfo* pActInfo)
		{
			m_pActInfo = pActInfo;
		}


		void OnActivate (SActivationInfo* pActInfo)
		{
			// Invert the mouse pitch?
			cl_tpvMouseInvertPitch = GetPortBool (pActInfo, EIP_MOUSE_INVERT_PITCH);

			// View pitch / yaw limitations.
			cl_tpvPitchMin = GetPortFloat (pActInfo, EIP_VIEW_PITCH_MIN);
			cl_tpvPitchMax = GetPortFloat (pActInfo, EIP_VIEW_PITCH_MAX);
			cl_tpvYawMin = GetPortFloat (pActInfo, EIP_VIEW_YAW_MIN);
			cl_tpvYawMax = GetPortFloat (pActInfo, EIP_VIEW_YAW_MAX);

			// Filter pitch and yaw adjustments below this threshhold (radians).
			cl_tpvPitchFilter = GetPortFloat (pActInfo, EIP_PITCH_FILTER);
			cl_tpvYawFilter = GetPortFloat (pActInfo, EIP_YAW_FILTER);

			// Mouse sensitivity for pitching and yawing.
			cl_tpvPitchYawSensitivity = GetPortFloat (pActInfo, EIP_PITCH_YAW_SENSITIVITY);

			// Zoom controls.
			cl_tpvZoom = GetPortFloat (pActInfo, EIP_ZOOM);
			cl_tpvZoomMin = GetPortFloat (pActInfo, EIP_ZOOM_MIN);
			cl_tpvZoomMax = GetPortFloat (pActInfo, EIP_ZOOM_MAX);
			cl_tpvZoomStep = GetPortFloat (pActInfo, EIP_ZOOM_STEP);
			cl_tpvZoomSpeed = GetPortFloat (pActInfo, EIP_ZOOM_LERP_SPEED);

			// Freelook?
			cl_tpvFreelook = GetPortBool (pActInfo, EIP_FREELOOK);
		}
	};
}

REGISTER_FLOW_NODE_EX ("Chrysalis:Camera:Settings", Chrysalis::CCameraSettingsNode, CCameraSettingsNode);
#pragma once

#include <Chrysalis/Player/Camera/Camera.h>

class CCameraPose;


namespace Chrysalis
{
	class ActionRPGCamera : public ICamera
	{
	public:
		ActionRPGCamera ();


		/**
		Enables the camera.
		 */
		virtual void EnableUpdates ();


		/**
		Disables the camera.
		 */
		virtual void DisableUpdates ();


		/**
		Performs all the calculations required to position the camera for the present frame.
		
		\param	clientPlayer	The client player.
		\param	viewParams  	Options for controlling the view.
		\param	frameTime   	The length of this frame expressed as a fraction of a second.
		
		\return	A CCameraPose indicating the desired camera placement.
		 */
		virtual CCameraPose UpdateView (const CPlayer& clientPlayer, SViewParams& viewParams, float frameTime);


		/**
		Activates this camera.
		
		\param	clientPlayer	The client player.
		 */
		virtual void Activate (const CPlayer& clientPlayer);


		/**
		Deactivates this camera.
		
		\param	clientPlayer	The client player.
		 */
		virtual void Deactivate (const CPlayer& clientPlayer);


		/**
		The camera moves to the present location of a given entity and then attaches itself to the
		entity. It remains attached until you move outside it's leash range.
		
		\param [in,out]	pEntity	If non-null, the entity.
		 */
		void AttachToEntity (IEntity* const pEntity);

		// Access the TargetPositionBone
		const string& GetTargetPositionBone (void) const { return(m_targetBoneName); };
		void SetTargetPositionBone (const string& targetPositionBone) { m_targetBoneName = targetPositionBone; };

		// Access the TargetViewAimBone
		const string& GetTargetViewAimBone (void) const { return(m_targetAimBoneName); };
		void SetTargetViewAimBone (const string& targetViewAimBone)	{ m_targetAimBoneName = targetViewAimBone; };

		// Access the TargetDistance
		float GetTargetDistance (void) const { return(m_targetDistance); };
		void SetTargetDistance (float targetDistance) { m_targetDistance = targetDistance; };

		// Access the ReversePitchTilt
		float GetReversePitchTilt (void) const { return(m_reversePitchTilt); };
		void SetReversePitchTilt (float reversePitchTilt) { m_reversePitchTilt = reversePitchTilt; };

		// Access the ViewPositionOffset
		const Vec3& GetViewPositionOffset (void) const { return(m_viewPositionOffset); };
		void SetViewPositionOffset (const Vec3& viewPositionOffset) { m_viewPositionOffset = viewPositionOffset; };

		// Access the AimPositionOffset
		const Vec3& GetAimPositionOffset (void) const { return(m_aimPositionOffset); };
		void SetAimPositionOffset (const Vec3& aimPositionOffset) { m_aimPositionOffset = aimPositionOffset; };

		// Access the SLERP speed.
		float GetSlerpSpeed (void) const { return(m_slerpSpeed); };
		void SetSlerpSpeed (float slerpSpeed) { m_slerpSpeed = slerpSpeed; };

	protected:

		/**
		Applies the pitch and yaw delta from the XBox controller. This needs to be called every
		update if we wish to support input from an XBox controller.
		
		\param	frameTime	The frame time.
		 */
		virtual void ApplyXIPitchYaw (float frameTime);


		/**
		Resets the results of all player based camera movements back to their defaults.
		 */
		virtual void ResetMovements ();

	private:

		/**
		Determines the positions for target entity and aim.
		
		\param [in,out]	pEntity					If non-null, the entity.
		\param	targetBone						The name of a bone which will act as the locus of our
												camera. The camera is attached to this bone on the entity, if
												it exists and all movements are relative to this location.
		\param	targetAimBone					The name of a bone at which to aim the camera. This
												provides the ability to aim the camera at a different
												position on the entity to where you are attached e.g. your
												camera is attached to the player's spine but you aim the
												camera towards their head.
		\param [in,out]	vecTargetPosition   	The target's position will be returned in this parameter.
		\param [in,out]	vecTargetAimPosition	The target's aim position will be returned in this
												parameter.
		 */
		void GetTargetPositions (IEntity* const pEntity,
			string targetBone, string targetAimBone,
			Vec3& vecTargetPosition, Vec3& vecTargetAimPosition);


		/**
		Performs a very simple collision detection. The camera view position is updated to the best
		viewing location based on the results of the raycasting.

		\param	Goal				  	The goal.
		\param [in,out]	CameraPosition	The camera position.

		\return	true if it succeeds, false if it fails.
		*/
		bool CollisionDetection (const Vec3& Goal, Vec3& CameraPosition);


		/**
		When the camera is attached, this is the entity it is attached onto.
		 */
		IEntity* m_pEntity;


		/**
		/ Name of the target bone.
		 */
		string m_targetBoneName;


		/**
		Name of the target aim bone.
		 */
		string m_targetAimBoneName;


		/**
		Preferred distance in metres from the view camera to the target.
		 */
		float m_targetDistance;


		/**
		Position of the camera during the last update.
		 */
		Vec3 m_vecLastPosition;


		/**
		Rotation of the camera during the last update.
		 */
		Quat m_quatLastTargetRotation;


		/**
		Initial rotation of the camera in reference to the entity it is attached onto.
		 */
		Quat m_quatTargetRotation;


		/**
		A factor that applies pitch in the direction the camera is facing. The effect of this is to
		tilt it towards or away from the orbit centre.
		 */
		float m_reversePitchTilt;


		/**
		A translation vector which is applied after the camera is initially positioned. This provides
		for 'over the shoulder' views of the target actor.
		 */
		Vec3 m_viewPositionOffset;


		/**
		A translation vector which is applied after the camera is initially positioned. This provides
		for 'over the shoulder' views of the target actor.
		*/
		Vec3 m_aimPositionOffset;


		/**
		The last known position of the camera target.
		 */
		Vec3 m_vecLastTargetPosition;


		/**
		The last known position of the camera aim target.
		 */
		Vec3 m_vecLastTargetAimPosition;


		/**
		The last rotation value for the view goal. This is used in the SLERP calculations for camera
		smoothing through rotations.
		 */
		Quat m_quatLastViewRotation;


		/**
		The speed at which we apply spherical linear interpolation to the camera rotations.
		 */
		float m_slerpSpeed;


		/**
		Set to true when you want to avoid using interpolation for the update next frame. There are
		times when we don't have a reasonable value for the last camera position / rotation and this
		allows us to get around those awkward moments.
		*/
		bool m_skipInterpolation;
	};
}

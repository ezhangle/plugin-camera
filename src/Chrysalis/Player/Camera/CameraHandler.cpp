#include <StdAfx.h>

#include <Chrysalis/Player/Camera/CameraHandler.h>
#include <Chrysalis/Player/Camera/Cvars.h>
#include <ISystem.h>


#define FILTER_ACTION(filter,action) pActionMapManager->GetActionFilter( filter )->Filter( action );


namespace Chrysalis
{
	// Goal value for zoom distance. FIX: unless this is made a member of CCameraHandler it will
	// not work properly in multi-camera / player situations.
	float m_zoomGoal;

	CCameraHandler::CCameraHandler ()
	{
		m_zoomGoal = cl_tpvZoom;
	}


	CCameraHandler::~CCameraHandler ()
	{
		// Release (true);
	}


	//
	// ISystemEventListener
	//

	void CCameraHandler::OnSystemEvent (ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam)
	{
		switch (event)
		{
			case ESYSTEM_EVENT_GAME_POST_INIT:
				RegisterCvars ();
				RegisterActionMaps ();
				break;

			case ESYSTEM_EVENT_SHUTDOWN:
				Shutdown ();
				break;
		}
	}

	//
	// IPluginListener
	//

	void CCameraHandler::Init ()
	{
		CRY_ASSERT (gEnv);
		CRY_ASSERT (gEnv->pGame);
		CRY_ASSERT (gEnv->pGame->GetIGameFramework ());
		CRY_ASSERT (gEnv->pGame->GetIGameFramework ()->GetISystem ());

		// Register Game Objects
		if (gEnv && gEnv->pGame && gEnv->pGame->GetIGameFramework ())
		{
			// Listen for system events.
			gEnv->pGame->GetIGameFramework ()->GetISystem ()->GetISystemEventDispatcher ()->RegisterListener (this);

			// Listen for game events.
			gEnv->pGame->GetIGameFramework ()->RegisterListener (this, "ThirdPersonCamera", FRAMEWORKLISTENERPRIORITY_DEFAULT);
		}

		ButtonPanningMask = 0;
	}


	void CCameraHandler::Shutdown ()
	{
		UnregisterCvars ();
		UnregisterActionMaps ();
	}


	void CCameraHandler::RegisterCvars ()
	{
		RegisterCameraCvars ();
	}


	void CCameraHandler::UnregisterCvars ()
	{
		UnregisterCameraCvars ();
	}


	void CCameraHandler::RegisterActionMaps ()
	{
		if (gEnv && gEnv->pGame && gEnv->pGame->GetIGameFramework ()->GetIActionMapManager ())
		{
			// Register our action map listener.
			IActionMapManager* pActionMapManager = gEnv->pGame->GetIGameFramework ()->GetIActionMapManager ();
			pActionMapManager->AddExtraActionListener (this);

			// Declare the set of actions we need to handle.
			ActionId tpv_ZoomIn ("tpv_zoom_in");
			ActionId tpv_ZoomOut ("tpv_zoom_out");
			ActionId v_tpv_ZoomIn ("v_tpv_zoom_in");
			ActionId v_tpv_ZoomOut ("v_tpv_zoom_out");
			ActionId tpv_MouseLook ("tpv_mouselook");
			ActionId tpv_MouseTurnLook ("tpv_mouseturnlook");
			ActionId tpv_ToggleFreelook ("tpv_togglefreelook");
			ActionId rotateYaw ("rotateyaw");
			ActionId rotatePitch ("rotatepitch");
			ActionId xi_rotateYaw ("xi_rotateyaw");
			ActionId xi_rotatePitch ("xi_rotatepitch");
			ActionId tpv_PanLeft ("tpv_panleft");
			ActionId tpv_PanRight ("tpv_panright");
			ActionId tpv_PanForward ("tpv_panforward");
			ActionId tpv_PanBack ("tpv_panback");

			// Filter the zoom in.
			FILTER_ACTION ("no_move", tpv_ZoomIn);
			FILTER_ACTION ("no_mouse", tpv_ZoomIn);
			FILTER_ACTION ("tutorial_no_move", tpv_ZoomIn);
			FILTER_ACTION ("warning_popup", tpv_ZoomIn);
			FILTER_ACTION ("cutscene_player_moving", tpv_ZoomIn);
			FILTER_ACTION ("cutscene_train", tpv_ZoomIn);
			FILTER_ACTION ("scoreboard", tpv_ZoomIn);
			FILTER_ACTION ("infiction_menu", tpv_ZoomIn);
			FILTER_ACTION ("mp_weapon_customization_menu", tpv_ZoomIn);
			FILTER_ACTION ("ledge_grab", tpv_ZoomIn);

			// Filter the zoom out.
			FILTER_ACTION ("no_move", tpv_ZoomOut);
			FILTER_ACTION ("no_mouse", tpv_ZoomOut);
			FILTER_ACTION ("tutorial_no_move", tpv_ZoomOut);
			FILTER_ACTION ("warning_popup", tpv_ZoomOut);
			FILTER_ACTION ("cutscene_player_moving", tpv_ZoomOut);
			FILTER_ACTION ("cutscene_train", tpv_ZoomOut);
			FILTER_ACTION ("scoreboard", tpv_ZoomOut);
			FILTER_ACTION ("infiction_menu", tpv_ZoomOut);
			FILTER_ACTION ("mp_weapon_customization_menu", tpv_ZoomOut);
			FILTER_ACTION ("ledge_grab", tpv_ZoomOut);

			// Filter the vehicle zoom in.
			FILTER_ACTION ("no_move", v_tpv_ZoomIn);
			FILTER_ACTION ("no_mouse", v_tpv_ZoomIn);
			FILTER_ACTION ("tutorial_no_move", v_tpv_ZoomIn);
			FILTER_ACTION ("warning_popup", v_tpv_ZoomIn);
			FILTER_ACTION ("cutscene_player_moving", v_tpv_ZoomIn);
			FILTER_ACTION ("cutscene_train", v_tpv_ZoomIn);
			FILTER_ACTION ("scoreboard", v_tpv_ZoomIn);
			FILTER_ACTION ("infiction_menu", v_tpv_ZoomIn);
			FILTER_ACTION ("mp_weapon_customization_menu", v_tpv_ZoomIn);
			FILTER_ACTION ("ledge_grab", v_tpv_ZoomIn);

			// Filter the vehicle zoom out.
			FILTER_ACTION ("no_move", v_tpv_ZoomOut);
			FILTER_ACTION ("no_mouse", v_tpv_ZoomOut);
			FILTER_ACTION ("tutorial_no_move", v_tpv_ZoomOut);
			FILTER_ACTION ("warning_popup", v_tpv_ZoomOut);
			FILTER_ACTION ("cutscene_player_moving", v_tpv_ZoomOut);
			FILTER_ACTION ("cutscene_train", v_tpv_ZoomOut);
			FILTER_ACTION ("scoreboard", v_tpv_ZoomOut);
			FILTER_ACTION ("infiction_menu", v_tpv_ZoomOut);
			FILTER_ACTION ("mp_weapon_customization_menu", v_tpv_ZoomOut);
			FILTER_ACTION ("ledge_grab", v_tpv_ZoomOut);

			// Filter the pan left.
			FILTER_ACTION ("no_move", tpv_PanLeft);
			FILTER_ACTION ("no_mouse", tpv_PanLeft);
			FILTER_ACTION ("tutorial_no_move", tpv_PanLeft);
			FILTER_ACTION ("warning_popup", tpv_PanLeft);
			FILTER_ACTION ("cutscene_player_moving", tpv_PanLeft);
			FILTER_ACTION ("cutscene_train", tpv_PanLeft);
			FILTER_ACTION ("scoreboard", tpv_PanLeft);
			FILTER_ACTION ("infiction_menu", tpv_PanLeft);
			FILTER_ACTION ("mp_weapon_customization_menu", tpv_PanLeft);
			FILTER_ACTION ("ledge_grab", tpv_PanLeft);

			// Filter the pan right.
			FILTER_ACTION ("no_move", tpv_PanRight);
			FILTER_ACTION ("no_mouse", tpv_PanRight);
			FILTER_ACTION ("tutorial_no_move", tpv_PanRight);
			FILTER_ACTION ("warning_popup", tpv_PanRight);
			FILTER_ACTION ("cutscene_player_moving", tpv_PanRight);
			FILTER_ACTION ("cutscene_train", tpv_PanRight);
			FILTER_ACTION ("scoreboard", tpv_PanRight);
			FILTER_ACTION ("infiction_menu", tpv_PanRight);
			FILTER_ACTION ("mp_weapon_customization_menu", tpv_PanRight);
			FILTER_ACTION ("ledge_grab", tpv_PanRight);

			// Filter the pan forward.
			FILTER_ACTION ("no_move", tpv_PanForward);
			FILTER_ACTION ("no_mouse", tpv_PanForward);
			FILTER_ACTION ("tutorial_no_move", tpv_PanForward);
			FILTER_ACTION ("warning_popup", tpv_PanForward);
			FILTER_ACTION ("cutscene_player_moving", tpv_PanForward);
			FILTER_ACTION ("cutscene_train", tpv_PanForward);
			FILTER_ACTION ("scoreboard", tpv_PanForward);
			FILTER_ACTION ("infiction_menu", tpv_PanForward);
			FILTER_ACTION ("mp_weapon_customization_menu", tpv_PanForward);
			FILTER_ACTION ("ledge_grab", tpv_PanForward);

			// Filter the pan back.
			FILTER_ACTION ("no_move", tpv_PanBack);
			FILTER_ACTION ("no_mouse", tpv_PanBack);
			FILTER_ACTION ("tutorial_no_move", tpv_PanBack);
			FILTER_ACTION ("warning_popup", tpv_PanBack);
			FILTER_ACTION ("cutscene_player_moving", tpv_PanBack);
			FILTER_ACTION ("cutscene_train", tpv_PanBack);
			FILTER_ACTION ("scoreboard", tpv_PanBack);
			FILTER_ACTION ("infiction_menu", tpv_PanBack);
			FILTER_ACTION ("mp_weapon_customization_menu", tpv_PanBack);
			FILTER_ACTION ("ledge_grab", tpv_PanBack);

			// Only add the handlers once.
			// TODO: look into multiple instances and how it affects this whole section of code.
			if (m_actionHandler.GetNumHandlers () == 0)
			{
				// Zoom handlers.
				m_actionHandler.AddHandler (tpv_ZoomIn, &CCameraHandler::OnActionZoomIn);
				m_actionHandler.AddHandler (tpv_ZoomOut, &CCameraHandler::OnActionZoomOut);

				// Mouse yaw and pitch handlers.
				m_actionHandler.AddHandler (rotateYaw, &CCameraHandler::OnActionRotateYaw);
				m_actionHandler.AddHandler (rotatePitch, &CCameraHandler::OnActionRotatePitch);

				// XBox controller yaw and pitch handlers.
				m_actionHandler.AddHandler (xi_rotateYaw, &CCameraHandler::OnActionXIRotateYaw);
				m_actionHandler.AddHandler (xi_rotatePitch, &CCameraHandler::OnActionXIRotatePitch);

				// Zoom while in vehicles maps to the same actions.
				m_actionHandler.AddHandler (v_tpv_ZoomIn, &CCameraHandler::OnActionZoomIn);
				m_actionHandler.AddHandler (v_tpv_ZoomOut, &CCameraHandler::OnActionZoomOut);

				// Mouse look modes.
				m_actionHandler.AddHandler (tpv_MouseLook, &CCameraHandler::OnActionMouseLook);
				m_actionHandler.AddHandler (tpv_MouseTurnLook, &CCameraHandler::OnActionMouseTurnLook);

				// Toggle freelook.
				m_actionHandler.AddHandler (tpv_ToggleFreelook, &CCameraHandler::OnActionToggleFreelook);

				// Movement.
				m_actionHandler.AddHandler (tpv_PanLeft, &CCameraHandler::OnActionPanLeft);
				m_actionHandler.AddHandler (tpv_PanRight, &CCameraHandler::OnActionPanRight);
				m_actionHandler.AddHandler (tpv_PanForward, &CCameraHandler::OnActionPanForward);
				m_actionHandler.AddHandler (tpv_PanBack, &CCameraHandler::OnActionPanBack);
			}
		}
	}


	void CCameraHandler::UnregisterActionMaps ()
	{
		if (gEnv && gEnv->pGame && gEnv->pGame->GetIGameFramework ())
		{
			if (gEnv->pGame->GetIGameFramework ()->GetIActionMapManager ())
				gEnv->pGame->GetIGameFramework ()->GetIActionMapManager ()->RemoveExtraActionListener (this);
		}
	}

	//
	// Action handlers.
	//

	bool CCameraHandler::OnActionZoomIn (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		m_zoomGoal = MAX (cl_tpvZoom - cl_tpvZoomStep, cl_tpvZoomMin);

		return false;
	}


	bool CCameraHandler::OnActionZoomOut (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		m_zoomGoal = MIN (cl_tpvZoom + cl_tpvZoomStep, cl_tpvZoomMax);

		return false;
	}


	bool CCameraHandler::OnActionRotateYaw (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		if (cl_tpvMouseLook || cl_tpvFreelook)
		{
			// Sensible scaling value for rotations in degrees.
			float mouseSensitivity;
			mouseSensitivity = 0.00333f * MAX (0.01f, (gEnv->pConsole->GetCVar ("cl_sensitivity")->GetFVal ()) * cl_tpvPitchYawSensitivity);

			// Filter small movements out to help stabilise the camera.
			float rotationRadians = value * mouseSensitivity;
			if (abs (rotationRadians) < cl_tpvYawFilter)
				return false;

			// Add the yaw delta.
			cl_tpvViewYaw -= rotationRadians;

			// We want the values to circle around from 0.0 - 360.0
			if (cl_tpvViewYaw > 360.0f)
				cl_tpvViewYaw -= 360.0f;

			if (cl_tpvViewYaw < 0.0f)
				cl_tpvViewYaw += 360.0f;

			// Limit the yaw movement according to the set max and min values.
			cl_tpvViewYaw = clamp_tpl (cl_tpvViewYaw, cl_tpvYawMin, cl_tpvYawMax);

			// Mouse panning.
			//mouseSensitivity = 0.0003f * MAX (0.01f, (gEnv->pConsole->GetCVar ("cl_sensitivity")->GetFVal()) * cl_tpvPanMouseSensitivity);
			//cl_tpvPanHorizontal += value * mouseSensitivity;
			//cl_tpvPanHorizontal = clamp_tpl (cl_tpvPanHorizontal, cl_tpvPanHorizontalMin, cl_tpvPanHorizontalMax);
		}

		return false;
	}


	bool CCameraHandler::OnActionRotatePitch (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		if (cl_tpvMouseLook || cl_tpvFreelook)
		{
			// Have they inverted their mouse control?
			if (gEnv->pConsole->GetCVar ("cl_invertMouse")->GetIVal ())
				value *= -1.0f;

			// Sensible scaling value for rotations in degrees.
			float mouseSensitivity;
			mouseSensitivity = 0.00333f * MAX (0.01f, (gEnv->pConsole->GetCVar ("cl_sensitivity")->GetFVal ()) * cl_tpvPitchYawSensitivity);

			// Filter small movements out to help stabilise the camera.
			float rotationRadians = value * mouseSensitivity;
			if (abs (rotationRadians) < cl_tpvPitchFilter)
				return false;

			// Add the delta, taking into account mouse inversion.  Clamp the result.
			float invertYAxis = cl_tpvMouseInvertPitch ? -1.0 : 1.0;
			cl_tpvViewPitch += rotationRadians * invertYAxis;
			cl_tpvViewPitch = clamp_tpl (cl_tpvViewPitch, cl_tpvPitchMin, cl_tpvPitchMax);

			// Mouse panning.
			//mouseSensitivity = 0.0003f * MAX (0.01f, (gEnv->pConsole->GetCVar ("cl_sensitivity")->GetFVal()) * cl_tpvPanMouseSensitivity);
			//cl_tpvPanVertical += value * mouseSensitivity;
			//cl_tpvPanVertical = clamp_tpl (cl_tpvPanVertical, cl_tpvPanVerticalMin, cl_tpvPanVerticalMax);
		}

		return false;
	}


	// XBox controller rotation is handled differently. Movements on the thumb stick set a value for
	// rotation that should be applied every frame update.
	bool CCameraHandler::OnActionXIRotateYaw (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		cl_tpvXIYawDelta = value;

		return false;
	}


	// Xbox controller rotation is handled differently. Movements on the thumb stick set a value for
	// rotation that should be applied every frame update.
	bool CCameraHandler::OnActionXIRotatePitch (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		cl_tpvXIPitchDelta = value;

		return false;
	}


	// Switches mouse look on and off according to input from the user.
	bool CCameraHandler::OnActionMouseLook (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		if (value > 0.0f)
			cl_tpvMouseLook = eTpvMouseLook_On;
		else
			cl_tpvMouseLook = eTpvMouseLook_Off;

		return false;
	}


	// Switches mouse look and turn on and off according to input from the user.
	bool CCameraHandler::OnActionMouseTurnLook (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		if (value > 0.0f)
			cl_tpvMouseLook = eTpvMouseLook_TurnPlayer;
		else
			cl_tpvMouseLook = eTpvMouseLook_Off;

		return false;
	}


	bool CCameraHandler::OnActionToggleFreelook (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		if (cl_tpvFreelook)
			cl_tpvFreelook = false;
		else
			cl_tpvFreelook = true;

		return false;
	}


	bool CCameraHandler::OnActionPanLeft (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		if (activationMode == eAAM_OnRelease)
			ButtonPanningMask &= ~ePanningMask_Left;
		else
			ButtonPanningMask |= ePanningMask_Left;

		return false;
	}


	bool CCameraHandler::OnActionPanRight (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		if (activationMode == eAAM_OnRelease)
			ButtonPanningMask &= ~ePanningMask_Right;
		else
			ButtonPanningMask |= ePanningMask_Right;

		return false;
	}


	bool CCameraHandler::OnActionPanForward (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		if (activationMode == eAAM_OnRelease)
			ButtonPanningMask &= ~ePanningMask_Forward;
		else
			ButtonPanningMask |= ePanningMask_Forward;

		return false;
	}


	bool CCameraHandler::OnActionPanBack (EntityId entityId, const ActionId& actionId, int activationMode, float value)
	{
		if (activationMode == eAAM_OnRelease)
			ButtonPanningMask &= ~ePanningMask_Back;
		else
			ButtonPanningMask |= ePanningMask_Back;

		return false;
	}


	void CCameraHandler::OnAction (const ActionId& action, int activationMode, float value)
	{
		m_actionHandler.Dispatch (this, 0, action, activationMode, value);
	}


	void CCameraHandler::OnPostUpdate (float fDeltaTime)
	{
		Interpolate (cl_tpvZoom, m_zoomGoal, cl_tpvZoomSpeed, fDeltaTime);

		// TODO: change FoV
		// fov = DEG2RAD (25.0f + 55.0f * cl_tpvZoom);
	}


	void CCameraHandler::ResetMovements ()
	{
		// Zoom can default to it's mid-point.
		m_zoomGoal = cl_tpvZoom = (cl_tpvZoomMax + cl_tpvZoomMin) / 2;

		// Pitch and yaw can default to their mid-points.
		cl_tpvViewPitch = (cl_tpvPitchMax + cl_tpvPitchMin) / 2;
		cl_tpvViewYaw = (cl_tpvYawMax + cl_tpvYawMin) / 2;

		// Horizontal and vertical pan can default to their mid-points.
		cl_tpvPanHorizontal = (cl_tpvPanHorizontalOffsetMax + cl_tpvPanHorizontalOffsetMin) / 2;
		cl_tpvPanVertical = (cl_tpvPanVerticalOffsetMax + cl_tpvPanVerticalOffsetMin) / 2;
	}
}
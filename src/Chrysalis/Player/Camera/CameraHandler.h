#pragma once


namespace Chrysalis
{
	/**
	A bitset of the possible directions that the camera may be panned towards.
	 */
	enum EPanningMask
	{
		ePanningMask_Forward = (1 << 0),
		ePanningMask_Back = (1 << 1),
		ePanningMask_Left = (1 << 2),
		ePanningMask_Right = (1 << 3)
	};


	/**
	Values that represent ETpvMouseLook.
	 */
	enum ETpvMouseLook
	{
		eTpvMouseLook_Off = 0,
		eTpvMouseLook_On = 1,
		eTpvMouseLook_TurnPlayer = 2
	};


	/**
	Making the plugin conform to this interface will ease transition to Linux and other platforms
	when they become available.
	 */
	struct IPluginListener
	{
		virtual ~IPluginListener () {};
		virtual void Init () = 0;
	};


	/**
	A camera handler class hooks into the system and input events. It handles registration of new
	actions, cvars and interpreting player input into camera movements.
	 */
	class CCameraHandler :
		public ISystemEventListener,
		public IGameFrameworkListener,
		public IActionListener,
		public IPluginListener
	{
	public:
		CCameraHandler ();
		~CCameraHandler ();

		// ISystemEventListener
		virtual void OnSystemEventAnyThread (ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam) {}
		virtual void OnSystemEvent (ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam);

		// IGameFrameworkListener
		virtual void OnPostUpdate (float fDeltaTime);
		virtual void OnSaveGame (ISaveGame* pSaveGame) {};
		virtual void OnLoadGame (ILoadGame* pLoadGame) {};
		virtual void OnLevelEnd (const char* nextLevel) {};
		virtual void OnActionEvent (const SActionEvent& event) {};
		virtual void OnPreRender () {};

		// IActionListener
		virtual void OnAction (const ActionId& action, int activationMode, float value);

		// IPluginListener
		virtual void Init ();

		// Resets the results of all player based camera movements back to their defaults.
		static void ResetMovements ();

	private:
		// A static handler for the actions we are interested in hooking.
		TActionHandler<CCameraHandler> m_actionHandler;


		/**
		Shuts down this object and frees any resources it is using.
		 */
		void Shutdown ();


		/**
		Registers the cvars.
		 */
		void RegisterCvars ();


		/**
		Unregisters the cvars.
		 */
		void UnregisterCvars ();


		/**
		Registers the action maps.
		 */
		void RegisterActionMaps ();


		/**
		Unregisters the action maps.
		 */
		void UnregisterActionMaps ();


		/**
		Action handler for mouse zooming in.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionZoomIn (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for mouse zooming out.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionZoomOut (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for mouse yawing.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionRotateYaw (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for mouse pitching.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionRotatePitch (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for XBox controller yawing.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionXIRotateYaw (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for XBox controller pitching.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionXIRotatePitch (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for pressing the mouse look key.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionMouseLook (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for pressing the mouse look and turn key.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionMouseTurnLook (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for toggling mouse freelook mode.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionToggleFreelook (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for pressing a key for panning left.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionPanLeft (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for pressing a key for panning right.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionPanRight (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for pressing a key for panning forward.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionPanForward (EntityId entityId, const ActionId& actionId, int activationMode, float value);


		/**
		Action handler for pressing a key for panning backward.
		
		\param	entityId	  	Identifier for the entity.
		\param	actionId	  	Identifier for the action.
		\param	activationMode	The activation mode.
		\param	value		  	The value.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool OnActionPanBack (EntityId entityId, const ActionId& actionId, int activationMode, float value);
	};
}

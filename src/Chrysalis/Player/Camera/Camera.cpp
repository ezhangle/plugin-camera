#include "StdAfx.h"
#include "Player.h"
#include <Chrysalis/Player/Camera/Camera.h>


namespace Chrysalis
{
	ICamera::ICamera () :
		m_isEnabled (false),
		m_isDebugAllowed (false),
		m_isBlendingOff (false),
		m_disableDrawNearest (false)
	{
	}

	ICamera::~ICamera ()
	{
	}

	/**
	Activates this camera.

	\param	clientPlayer	The client player.
	*/
	void ICamera::ActivateMode (const CPlayer &clientPlayer)
	{
		m_isBlendingOff = false;

		if (m_disableDrawNearest)
		{
			SetDrawNearestFlag (clientPlayer, false);
		}

		Activate (clientPlayer);
	}


	/**
	Deactivates this camera.

	\param	clientPlayer	The client player.
	*/
	void ICamera::DeactivateMode (const CPlayer &clientPlayer)
	{
		m_isBlendingOff = true;

		if (m_disableDrawNearest)
		{
			SetDrawNearestFlag (clientPlayer, true);
		}

		Deactivate (clientPlayer);
	}


	void ICamera::SetDrawNearestFlag (const CPlayer & clientPlayer, bool drawNearestFlag)
	{
		if (!clientPlayer.IsThirdPerson ())
		{
			IEntity* pPlayerEntity = clientPlayer.GetEntity ();
			if (pPlayerEntity)
			{
				uint32 entitySlotFlags = pPlayerEntity->GetSlotFlags (eIGS_FirstPerson);
				if (drawNearestFlag)
				{
					entitySlotFlags |= ENTITY_SLOT_RENDER_NEAREST;
				}
				else
				{
					entitySlotFlags &= ~ENTITY_SLOT_RENDER_NEAREST;
				}
				pPlayerEntity->SetSlotFlags (eIGS_FirstPerson, entitySlotFlags);
			}
		}
	}


	bool ICamera::CanTransition ()
	{
		return false;
	}


	void ICamera::SetCameraAnimationFactor (const AnimationSettings& animationSettings)
	{
	}


	void ICamera::GetCameraAnimationFactor (float& position, float& rotation)
	{
		position = 0.0f;
		rotation = 0.0f;
	}
}
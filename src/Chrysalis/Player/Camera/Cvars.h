#pragma once


namespace Chrysalis
{
	/**
	Registers the cvars.
	 */
	void RegisterCameraCvars ();


	/**
	Unregisters the cvars.
	 */
	void UnregisterCameraCvars ();

	/**
	Determine the level of zoom on the third person camera. Sensible values are from 0.0f up to
	100.0f, although it is capable of zooming beyond that.
	 */
	extern float cl_tpvZoom;


	/**
	The minimum value for camera zoom.
	 */
	extern float cl_tpvZoomMin;


	/**
	The maximum value for camera zoom.
	 */
	extern float cl_tpvZoomMax;


	/**
	Each zoom event in or out will alter the zoom factor, cl_tpvZoom, by this amount. Use lower
	values for more steps and higher values to zoom in / out faster with less steps.
	 */
	extern float cl_tpvZoomStep;


	/**
	When the zoom changes we interpolate between it's last value and the goal value. This
	provides for smoother movement on camera zooms. Higher values will interpolate faster than lower
	values.
	 */
	extern float cl_tpvZoomSpeed;


	/**
	Should we invert the Y axis for mouse camera movements? This is preferred by some players,
	particularly those using a flight yoke.
	 */
	extern int cl_tpvMouseInvertPitch;


	/**
	A delta value in degrees to apply to the camera's initial calculated pitch.
	 */
	extern float cl_tpvViewPitch;


	/**
	A delta value in degrees to apply to the camera's initial calculated yaw.
	 */
	extern float cl_tpvViewYaw;


	/**
	A delta value in degrees to apply to the camera's initial calculated pitch. This value comes
	from the XBox controller if one is present.
	 */
	extern float cl_tpvXIPitchDelta;


	/**
	A delta value in degrees to apply to the camera's initial calculated yaw. This value comes
	from the XBox controller if one is present.
	 */
	extern float cl_tpvXIYawDelta;


	/**
	Minimum pitch delta in degrees.
	 */
	extern float cl_tpvPitchMin;


	/**
	Maximum pitch delta in degrees.
	 */
	extern float cl_tpvPitchMax;


	/**
	Minimum yaw delta in degrees.
	 */
	extern float cl_tpvYawMin;


	/**
	Maximum yaw delta in degrees.
	 */
	extern float cl_tpvYawMax;


	/**
	Filter pitch adjustments below this threshold (radians). This is useful for removing
	slight amounts of jitter on mouse movements and XBox controllers, making it easier to perform
	precise movements in only one axis.
	 */
	extern float cl_tpvPitchFilter;


	/**
	Filter yaw adjustments below this threshold (radians). This is useful for removing
	slight amounts of jitter on mouse movements and XBox controllers, making it easier to perform
	precise movements in only one axis.
	*/
	extern float cl_tpvYawFilter;


	/**
	Mouse sensitivity for pitching and yawing. All pitch and yaw deltas will be factored by this value.
	 */
	extern float cl_tpvPitchYawSensitivity;


	/**
	When enabled this allows the camera to pan freely using mouse horizontal and vertical
	movements without holding down a key.
	*/
	extern int cl_tpvFreelook;


	/**
	When enabled, mouselook is on.
	*/
	extern int cl_tpvMouseLook;


	/**
	Horizontal distance the camera is panned from the present locus in world co-ordinates.
	 */
	extern float cl_tpvPanHorizontal;


	/**
	Vertical distance the camera is panned from the present locus in world co-ordinates.
	 */
	extern float cl_tpvPanVertical;


	/**
	Mouse sensitivity for panning. All camera pan deltas will be factored by this value.
	 */
	extern float cl_tpvPanMouseSensitivity;


	/**
	Speed in metres / sec to pan when using button / key panning.
	 */
	extern float cl_tpvPanKeySpeed;


	/**
	Maximum distance the camera is allowed to pan horizontally from the locus.
	 */
	extern float cl_tpvPanHorizontalOffsetMax;


	/**
	Minimum distance the camera is allowed to pan horizontally from the locus.
	*/
	extern float cl_tpvPanHorizontalOffsetMin;


	/**
	Maximum distance the camera is allowed to pan vertically from the locus.
	*/
	extern float cl_tpvPanVerticalOffsetMax;


	/**
	Minimum distance the camera is allowed to pan vertically from the locus.
	*/
	extern float cl_tpvPanVerticalOffsetMin;


	/**
	Map co-ordinate constraints. The camera may only pan around within this bounding box.
	 */
	extern float cl_tpvPanHorizontalMax;


	/**
	Map co-ordinate constraints. The camera may only pan around within this bounding box.
	 */
	extern float cl_tpvPanHorizontalMin;


	/**
	Map co-ordinate constraints. The camera may only pan around within this bounding box.
	 */
	extern float cl_tpvPanVerticalMax;


	/**
	Map co-ordinate constraints. The camera may only pan around within this bounding box.
	 */
	extern float cl_tpvPanVerticalMin;


	/**
	A bitset that holds the present depressed state for the left / right / forward / back buttons.
	 */
	extern int ButtonPanningMask;
}

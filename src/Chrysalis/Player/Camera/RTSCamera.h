#pragma once

#include <Chrysalis/Player/Camera/Camera.h>

class CCameraPose;


namespace Chrysalis
{
	class RTSCamera : public ICamera
	{
	public:

		/**
		Default constructor.
		 */
		RTSCamera ();


		/**
		Activates this camera.
		
		\param	clientPlayer	The client player.
		 */
		virtual void Activate (const CPlayer& clientPlayer);


		/**
		Deactivates this camera.
		
		\param	clientPlayer	The client player.
		 */
		virtual void Deactivate (const CPlayer& clientPlayer);


		/**
		Performs all the calculations required to position the camera for the present frame.
		
		\param	clientPlayer	The client player.
		\param	viewParams  	Options for controlling the view.
		\param	frameTime   	The length of this frame expressed as a fraction of a second.
		
		\return	A CCameraPose indicating the desired camera placement.
		 */
		virtual CCameraPose UpdateView (const CPlayer& clientPlayer, SViewParams& viewParams, float frameTime);


		/**
		The camera moves to the present location of a given entity and then attaches itself to the
		entity. It remains attached until you move outside it's leash range.
		
		\param [in,out]	pEntity	If non-null, the entity.
		 */
		void AttachToEntity (IEntity* const pEntity);


		/**
		The camera moves to the present location of a given entity. It is left in a detached state.
		
		\param [in,out]	pEntity	If non-null, the entity.
		 */
		void MoveToEntity (IEntity* const pEntity);


		/**
		The camera is moved to a new position on the map.
		
		\param	newPosition	The new position.
		 */
		void MoveToPosition (const Vec3& newPosition);

		// Access the TargetPositionBone
		const string& GetTargetPositionBone (void) const { return(m_targetBoneName); };
		void SetTargetPositionBone (const string& targetPositionBone) { m_targetBoneName = targetPositionBone; };

		// Access the TargetViewAimBone
		const string& GetTargetViewAimBone (void) const { return(m_targetAimBoneName); };
		void SetTargetViewAimBone (const string& targetViewAimBone)	{ m_targetAimBoneName = targetViewAimBone; };

		// Access the TargetDistance
		float GetTargetDistance (void) const { return(m_targetDistance); };
		void SetTargetDistance (float targetDistance) { m_targetDistance = targetDistance; };

		// Access the ReversePitchTilt
		float GetReversePitchTilt (void) const { return(m_reversePitchTilt); };
		void SetReversePitchTilt (float reversePitchTilt) { m_reversePitchTilt = reversePitchTilt; };

		// Access the ViewPositionOffset
		const Vec3& GetViewPositionOffset (void) const { return(m_viewPositionOffset); };
		void SetViewPositionOffset (const Vec3& postTransOffset) { m_viewPositionOffset = postTransOffset; };

		// Access the AimPositionOffset
		const Vec3& GetAimPositionOffset (void) const { return(m_aimPositionOffset); };
		void SetAimPositionOffset (const Vec3& aimPositionOffset) { m_aimPositionOffset = aimPositionOffset; };

		// Access the CameraGroundPosition
		const Vec3& GetCameraGroundPosition (void) const { return(m_cameraGroundPosition); };
		void SetCameraGroundPosition (const Vec3& cameraGroundPosition)
		{
			m_cameraGroundPosition = cameraGroundPosition;
			m_lastGroundHeight = m_groundHeight = DetermineGroundHeight (cameraGroundPosition);
		};

		// Access the GroundHeight
		float GetGroundHeight (void) const { return(m_groundHeight); };

		// Access the IsCameraAttached
		// TODO: make it also check it has a valid entity.
		bool IsCameraAttached (void) const { return(m_isCameraAttached); };

		// Access the CameraHeight
		float GetCameraHeight (void) const { return(m_cameraHeight); };
		void SetCameraHeight (float cameraHeight) { m_cameraHeight = cameraHeight; };

		// Access the GroundHuggingSpeed
		float GetGroundHuggingSpeed (void) const { return(m_groundHuggingSpeed); };
		void SetGroundHuggingSpeed (float groundHuggingSpeed) { m_groundHuggingSpeed = groundHuggingSpeed; };

		// Access the TargetPosition
		const Vec3& GetTargetPosition (void) const { return(m_vecTargetPosition); };

		// Access the TargetAimPosition
		const Vec3& GetTargetAimPosition (void) const { return(m_vecTargetAimPosition); };

		// Access the SLERP speed.
		float GetSlerpSpeed (void) const { return(m_slerpSpeed); };
		void SetSlerpSpeed (float slerpSpeed) { m_slerpSpeed = slerpSpeed; };

	protected:

		/**
		Applies the pitch and yaw delta from the XBox controller. This needs to be called every
		update if we wish to support input from an XBox controller.
		
		\param	frameTime	The frame time.
		 */
		virtual void ApplyXIPitchYaw (float frameTime);


		/**
		Resets the results of all player based camera movements back to their defaults.
		 */
		virtual void ResetMovements ();

	private:

		/**
		When the camera is attached, this is the entity it is attached onto.
		 */
		IEntity* m_pEntity;


		/**
		The name of the bone we will set as our locus.
		 */
		string m_targetBoneName;


		/**
		The name of the bone we will try and aim the camera towards.
		 */
		string m_targetAimBoneName;


		/**
		Preferred distance in metres to the view camera.
		 */
		float m_targetDistance;


		/**
		Position of the camera during the last update.
		 */
		Vec3 m_vecLastPosition;


		/**
		Rotation of the camera during the last update.
		 */
		Quat m_quatLastTargetRotation;


		/**
		Initial rotation of the camera in reference to the entity it is attached onto.
		 */
		Quat m_quatTargetRotation;


		/**
		A factor that applies pitch in the direction the camera is facing. The effect of this is to
		tilt it towards or away from the orbit centre.
		 */
		float m_reversePitchTilt;


		/**
		A translation vector which is applied after the camera is initially positioned. This provides
		for 'over the shoulder' views of the target actor.
		 */
		Vec3 m_viewPositionOffset;


		/**
		A translation vector which is applied after the camera is initially positioned. This provides
		for 'over the shoulder' views of the target actor.
		*/
		Vec3 m_aimPositionOffset;


		/**
		The locus to be used when we are not attached to an actor. The x,y shall be freely selectable
		while the z should be calculated based on height from the terrain.
		 */
		Vec3 m_cameraGroundPosition;


		/**
		Is the camera currently attached to an actor?
		 */
		bool m_isCameraAttached;


		/**
		When the camera is unattached it should float at this height above the terrain.
		 */
		float m_cameraHeight;


		/**
		The height of the ground.
		 */
		float m_groundHeight;


		/**
		The last height of the ground.
		 */
		float m_lastGroundHeight;


		/**
		Speed at which to interpolate between changes in ground height while panning.
		 */
		float m_groundHuggingSpeed;


		/**
		The world position of the target.
		 */
		Vec3 m_vecTargetPosition;


		/**
		The world position of the aim target.
		 */
		Vec3 m_vecTargetAimPosition;


		/**
		The last known position of the camera target.
		 */
		Vec3 m_vecLastTargetPosition;


		/**
		The last known position of the camera aim target.
		 */
		Vec3 m_vecLastTargetAimPosition;


		/**
		The last rotation value for the view goal. This is used in the SLERP calculations for camera
		smoothing through rotations.
		*/
		Quat m_quatLastViewRotation;


		/**
		The speed at which we apply spherical linear interpolation to the camera rotations.
		*/
		float m_slerpSpeed;


		/**
		Set to true when you want to avoid using interpolation for the update next frame. There are
		times when we don't have a reasonable value for the last camera position / rotation and this
		allows us to get around those awkward moments.
		*/
		bool m_skipInterpolation;


		/**
		Determines the height of the ground including entities.
		
		\param	startingPosition	The starting position. A ray trace is performed from a little above
									this position to prevent the camera clipping through large changes in
									terrain height.
		\param	extraHeight			(Optional) The ray will be shot from above the startingPosition by
									this amount in metres. The purpose of this is to allow the camera to
									track well over terrain with large changes in height.
		
		\return	The ground height, or the startingPosition height if the ground could not be found.
		 */
		float DetermineGroundHeight (const Vec3& startingPosition, float extraHeight = 0.0f) const;


		/**
		Determines the positions for target entity and aim.
		
		\param [in,out]	pEntity					If non-null, the entity.
		\param	targetBone						The name of a bone which will act as the locus of our
												camera. The camera is attached to this bone on the entity, if
												it exists and all movements are relative to this location.
		\param	targetAimBone					The name of a bone at which to aim the camera. This
												provides the ability to aim the camera at a different
												position on the entity to where you are attached e.g. your
												camera is attached to the player's spine but you aim the
												camera towards their head.
		\param [in,out]	vecTargetPosition   	The target's position will be returned in this parameter.
		\param [in,out]	vecTargetAimPosition	The target's aim position will be returned in this
												parameter.
		 */
		void GetTargetPositions (IEntity* const pEntity,
			string targetBone, string targetAimBone,
			Vec3& vecTargetPosition, Vec3& vecTargetAimPosition);


		/**
		Determine the panning offsets according to keypress / controller inputs.
		
		\param	frameTime	  	The frame time.
		\param	cameraRotation	The initial camera rotation is required in order to apply panning taking
								into account the present rotation of the camera.
		
		\return	true if it succeeds, false if it fails.
		 */
		Vec3 CalculateButtonPanning (float frameTime, const Quat& cameraRotation);


		/**
		Performs a very simple collision detection. The camera view position is updated to the best
		viewing location based on the results of the raycasting.
		
		\param	Goal				  	The goal.
		\param [in,out]	CameraPosition	The camera position.
		
		\return	true if it succeeds, false if it fails.
		 */
		bool CollisionDetection (const Vec3& Goal, Vec3& CameraPosition);


		/**
		Limit the target position to inside the map perimeter.
		
		\param [in,out]	vecTarget	The vector target.
		 */
		void LimitTargetRange (Vec3& vecTarget);


		/**
		Common code for moving and attaching to an entity.
		
		\param [in,out]	pEntity	If non-null, the entity.
		 */
		void MakeEntityOurTarget (IEntity* const pEntity);
	};
}

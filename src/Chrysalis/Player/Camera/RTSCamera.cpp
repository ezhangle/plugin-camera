#include <StdAfx.h>
#include <ISystem.h>
#include <Player.h>
#include <PlayerCamera.h>
#include <CameraModes.h>
#include <Chrysalis/Player/Camera/RTSCamera.h>
#include <Chrysalis/Player/Camera/CameraHandler.h>
#include <Chrysalis/Player/Camera/Cvars.h>


namespace Chrysalis
{
	RTSCamera::RTSCamera () :
		m_pEntity (nullptr),
		m_targetBoneName (""),
		m_targetAimBoneName (""),
		m_viewPositionOffset (Vec3 (ZERO)),
		m_aimPositionOffset (Vec3 (ZERO)),
		m_targetDistance (5.0f),
		m_reversePitchTilt (0.75f),
		m_vecLastPosition (Vec3 (ZERO)),
		m_quatLastTargetRotation (Quat (IDENTITY)),
		m_quatTargetRotation (Quat (IDENTITY)),
		m_cameraGroundPosition (Vec3 (ZERO)),
		m_isCameraAttached (false),
		m_cameraHeight (0.0f),
		m_groundHeight (0.0f),
		m_lastGroundHeight (0.0f),
		m_groundHuggingSpeed (0.1f),
		m_vecLastTargetPosition (Vec3 (ZERO)),
		m_vecLastTargetAimPosition (Vec3 (ZERO)),
		m_quatLastViewRotation (Quat (IDENTITY)),
		m_slerpSpeed (20.0f),
		m_skipInterpolation (true)
	{
	}


	void RTSCamera::Activate (const CPlayer& clientPlayer)
	{
		EnableUpdates ();
		CCameraHandler::ResetMovements ();
		m_skipInterpolation = true;

		// Give the camera an initial orientation based on the player's rotation.
		cl_tpvViewPitch = (cl_tpvPitchMax - cl_tpvPitchMin) / 2;
		cl_tpvViewPitch = 30;

		// Load up the ground position now, in case there's no entity attached.
		// FIX: find a workaround for this.
		//SetCameraGroundPosition (GetPortVec3 (pActInfo, EIP_NEW_POSITION));

		// Listen for PlayerCamera events.
		clientPlayer.GetPlayerCamera ()->RegisterCameraListener (this);

		// Throw them into third person if they aren't already.
		// TODO: this is a bit naughty, refactor it so CPlayerCamera handles the changeover.
		if (!clientPlayer.IsThirdPerson ())
			const_cast <CPlayer&> (clientPlayer).ToggleThirdPerson ();
	}


	void RTSCamera::Deactivate (const CPlayer& clientPlayer)
	{
		DisableUpdates ();

		// Stop listening for PlayerCamera events.
		clientPlayer.GetPlayerCamera ()->UnregisterCameraListener (this);
	}


	void RTSCamera::GetTargetPositions (IEntity* const pEntity,
		string targetBone, string targetAimBone,
		Vec3& vecTargetPosition, Vec3& vecTargetAimPosition)
	{
		if (pEntity)
		{
			// Default is simply to use the entity position.
			vecTargetAimPosition = vecTargetPosition = pEntity->GetPos ();

			// If they want to target a bone, we need to have an actor to work with.
			if ((targetBone.length () > 0) || (targetAimBone.length () > 0))
			{
				IActor* pActor = gEnv->pGame->GetIGameFramework ()->GetIActorSystem ()->GetActor (pEntity->GetId ());
				if (pActor)
				{
					// Get Animated Character
					IAnimatedCharacter* pAnimCharacter = pActor->GetAnimatedCharacter ();
					ICharacterInstance* pCharacter = pEntity->GetCharacter (0);

					// Get Skeleton Pose
					if (pAnimCharacter && pCharacter)
					{
						ISkeletonPose* pPose = pCharacter->GetISkeletonPose ();
						IDefaultSkeleton& targetSkeleton = pCharacter->GetIDefaultSkeleton ();

						if (pPose)
						{
							// The world position which acts as our camera target.
							int16 targetPositionBoneId = targetSkeleton.GetJointIDByName (targetBone);
							if (targetPositionBoneId >= 0)
								vecTargetPosition = (pAnimCharacter->GetAnimLocation () * pPose->GetAbsJointByID (targetPositionBoneId)).t;

							// The world position of the bone we wish to aim towards.
							int16 targetViewAimBoneId = targetSkeleton.GetJointIDByName (targetAimBone);
							if (targetViewAimBoneId >= 0)
								vecTargetAimPosition = (pAnimCharacter->GetAnimLocation () * pPose->GetAbsJointByID (targetViewAimBoneId)).t;
						}
					}
				}
			}
		}
	}


	float RTSCamera::DetermineGroundHeight (const Vec3& startingPosition, float extraHeight) const
	{
		float detectedHeight;

		detectedHeight = gEnv->p3DEngine->GetBottomLevel (startingPosition + Vec3 (0.0f, 0.0f, extraHeight), startingPosition.z + extraHeight,
			ent_terrain | ent_static | ent_rigid | ent_sleeping_rigid);

		if (detectedHeight > BOTTOM_LEVEL_UNKNOWN)
			return detectedHeight;
		else
			return startingPosition.z;
	}


	void RTSCamera::MakeEntityOurTarget (IEntity* const pEntity)
	{
		// Give the camera an initial orientation based on the entity's rotation.
		if (pEntity)
			m_quatTargetRotation = Quat::CreateRotationVDir (pEntity->GetForwardDir ());
		else
			m_quatTargetRotation = Quat::CreateIdentity ();
		m_quatLastTargetRotation = m_quatTargetRotation;

		EntityId entityId = pEntity->GetId ();
		m_skipInterpolation = true;

		// We set a new camera ground position based on the entity position and the present
		// offset from that position.
		GetTargetPositions (pEntity, m_targetBoneName, m_targetAimBoneName, m_vecTargetPosition, m_vecTargetAimPosition);
		m_cameraGroundPosition = m_vecTargetPosition;

		// Starting at the height they passed in, we search for the terrain height.
		m_lastGroundHeight = m_groundHeight = DetermineGroundHeight (m_cameraGroundPosition);
	}


	void RTSCamera::MoveToEntity (IEntity* const pEntity)
	{
		// We only move to the entity position without attaching to it.
		m_pEntity = nullptr;
		m_isCameraAttached = false;
		MakeEntityOurTarget (pEntity);
	}


	void RTSCamera::AttachToEntity (IEntity* const pEntity)
	{
		// Attach the camera to the entity.
		m_pEntity = pEntity;
		m_isCameraAttached = true;
		MakeEntityOurTarget (pEntity);
	}


	void RTSCamera::MoveToPosition (const Vec3& newPosition)
	{
		// Make sure the camera is no longer attached to an entity.
		m_pEntity = nullptr;
		m_isCameraAttached = false;
		m_skipInterpolation = true;

		// Location should just be an X,Y - we derive the Z.
		m_cameraGroundPosition = newPosition;

		// Starting at the height they passed in, we search for the terrain height.
		m_lastGroundHeight = m_groundHeight = DetermineGroundHeight (newPosition);
	}


	bool RTSCamera::CollisionDetection (const Vec3& Goal, Vec3& CameraPosition)
	{
		bool updatedCameraPosition = false;

		// Try and update where the player is aiming.
		// TODO: need to skip the player's geometry and if they are in a vehicle, that needs skipping too.
		ray_hit rayhit;
		static IPhysicalEntity* pSkipEnts [10];
		int nskip = 0;

		int hits = gEnv->pPhysicalWorld->RayWorldIntersection (Goal,
			CameraPosition - Goal,
			ent_all,
			rwi_stop_at_pierceable | rwi_colltype_any,
			&rayhit,
			1, pSkipEnts, nskip + 1);

		if (hits)
		{
			CameraPosition = rayhit.pt;
			updatedCameraPosition = true;
		}

		return updatedCameraPosition;
	}


	void RTSCamera::ApplyXIPitchYaw (float frameTime)
	{
		// Pitch.
		cl_tpvViewPitch += cl_tpvXIPitchDelta;
		cl_tpvViewPitch = clamp_tpl (cl_tpvViewPitch, cl_tpvPitchMin, cl_tpvPitchMax);

		// Yaw.
		cl_tpvViewYaw += cl_tpvXIYawDelta;

		// We want the values to circle around from 0.0 - 360.0
		if (cl_tpvViewYaw > 360.0f)
			cl_tpvViewYaw -= 360.0f;
		if (cl_tpvViewYaw < -0.0f)
			cl_tpvViewYaw += 360.0f;

		// Limit the yaw movement according to the set max and min values.
		cl_tpvViewYaw = clamp_tpl (cl_tpvViewYaw, cl_tpvYawMin, cl_tpvYawMax);
	}


	CCameraPose RTSCamera::UpdateView (const CPlayer& clientPlayer, SViewParams& viewParams, float frameTime)
	{
		CRY_ASSERT (gEnv);
		CRY_ASSERT (gEnv->pGame);
		CRY_ASSERT (gEnv->pGame->GetIGameFramework ());

		if (gEnv && gEnv->pGame && gEnv->pGame->GetIGameFramework () && m_isEnabled && !gEnv->IsCutscenePlaying ())
		{
			// Make sure we have an actor and are in third person mode.
			IActor* pClient = gEnv->pGame->GetIGameFramework ()->GetClientActor ();
			if (pClient && !pClient->IsThirdPerson ())
				return CCameraPose (viewParams.position, viewParams.rotation);

			// XBox controller yaw / pitch deltas need processing.
			ApplyXIPitchYaw (frameTime);

			// Calculate pitch and yaw movements to apply both prior to and after positioning the camera.
			Quat quatPreTransYP = Quat (Ang3 (DEG2RAD (cl_tpvViewPitch), 0.0f, DEG2RAD (cl_tpvViewYaw)));
			Quat quatPostTransYP = Quat::CreateRotationXYZ (Ang3 (DEG2RAD (cl_tpvViewPitch * m_reversePitchTilt), 0.0f, 0.0f));

			// The distance from the view to the target.
			float shapedZoom = (cl_tpvZoom * cl_tpvZoom) / (cl_tpvZoomMax * cl_tpvZoomMax);
			float zoomDistance = m_targetDistance * shapedZoom;

			// We need to check to see if our entity is about to become garbage.
			if (m_pEntity && m_pEntity->IsGarbage ())
			{
				// The entity is no longer valid so reset the pointer.
				m_pEntity = nullptr;
			}

			// If we lose access to the entity we need to detach the camera.
			if (m_pEntity && (m_pEntity->IsInvisible () || m_pEntity->IsHidden ()))
			{
				// Retrieve the last known position of the attached camera.
				// Is this part needed?
				m_vecTargetPosition = m_vecLastTargetPosition;
				m_vecTargetAimPosition = m_vecLastTargetAimPosition;

				// Detach.
				m_isCameraAttached = false;
			}

			if ((m_isCameraAttached) && (m_pEntity))
			{
				// Target and aim position come from the entity position.
				GetTargetPositions (m_pEntity, m_targetBoneName, m_targetAimBoneName, m_vecTargetPosition, m_vecTargetAimPosition);
			}
			else
			{
				// We try and set the new ground height from our current position, factoring in our last known
				// height. We need to give it a little extra height to slip over large
				// changes in terrain.
				m_groundHeight = m_lastGroundHeight;
				float preferredGroundHeight = DetermineGroundHeight (m_cameraGroundPosition, m_groundHeight + 128.0f);
				Interpolate (m_groundHeight, preferredGroundHeight, m_groundHuggingSpeed, frameTime);
				m_lastGroundHeight = m_groundHeight;

				// Target and aim position comes from the ground position. We toss away it's height component first.
				m_vecTargetPosition = Vec3 (m_cameraGroundPosition.x, m_cameraGroundPosition.y, m_groundHeight + m_cameraHeight);
				LimitTargetRange (m_vecTargetPosition);

				// Aim is the same as the target position.
				m_vecTargetAimPosition = m_vecTargetPosition;
			}

			// Calculate the target rotation and slerp it if we can.
			Quat quatTargetRotationGoal = m_quatTargetRotation * quatPreTransYP;
			Quat quatTargetRotation;
			if (m_skipInterpolation)
			{
				m_quatLastTargetRotation = quatTargetRotation = quatTargetRotationGoal;
			}
			else
			{
				quatTargetRotation = Quat::CreateSlerp (m_quatLastTargetRotation, quatTargetRotationGoal, frameTime * m_slerpSpeed);
				m_quatLastTargetRotation = quatTargetRotation;
			}

			// Work out where to place the new initial position. We will be using a unit vector facing forward Y
			// as the starting place and applying rotations from the target bone and player camera movements.
			Vec3 vecViewPosition;
			if (quatTargetRotation.IsIdentity ())
				vecViewPosition = m_vecTargetPosition + (Vec3 (0.0f, 1.0f, 0.0f) * zoomDistance);
			else
				vecViewPosition = m_vecTargetPosition + (quatTargetRotation * Vec3 (0.0f, 1.0f, 0.0f) * zoomDistance);

			// By default, we try and aim the camera at the target, taking into account
			// the current mouse yaw and pitch values. If, by chance, the target and the view position
			// are the same, then we cheat and use the initial target rotation - which should provide an
			// acceptable result, hopefully. Avoid this by not allowing to zoom too close to the target.
			Vec3 vecAimAtTarget = (m_vecTargetAimPosition - vecViewPosition).GetNormalized ();
			Quat quatViewRotationGoal = vecAimAtTarget.IsZero () ? quatTargetRotation : Quat::CreateRotationVDir (vecAimAtTarget);

			// Use a slerp to smooth out fast camera rotations.
			Quat quatViewRotation;
			if (m_skipInterpolation)
			{
				m_quatLastViewRotation = quatViewRotation = quatViewRotationGoal;
				m_skipInterpolation = false;
			}
			else
			{
				quatViewRotation = Quat::CreateSlerp (m_quatLastViewRotation, quatViewRotationGoal, frameTime * m_slerpSpeed);
				m_quatLastViewRotation = quatViewRotation;
			}

			// Determine the result of any camera panning done using button controls.
			Vec3 vecDeltaPan = CalculateButtonPanning (frameTime, quatViewRotation);

			// If they break the leash we want to attach the camera to an x,y position instead.
			bool brokeLeash = m_isCameraAttached && vecDeltaPan.GetLength () > 0.0;
			if (brokeLeash)
			{
				// We set a new camera ground position based on the entity position and the present
				// offset from that position.
				m_cameraGroundPosition = m_vecTargetPosition;

				// We need a new camera height, at this point it's best to take the present height above ground
				// of the entity we were attached to.
				m_lastGroundHeight = m_groundHeight = DetermineGroundHeight (m_cameraGroundPosition + Vec3 (0.0f, 0.0f, m_vecTargetPosition.z + 128.0f));
				// TODO: figure out handling of height when breaking leash. Maybe we reset interp?
				//m_cameraHeight = m_vecTargetPosition.z - m_groundHeight;

				// The camera is no longer considered to be attached to the entity.
				m_isCameraAttached = false;
			}

			// Apply a final translation to both the view position and the aim position.
			vecViewPosition += quatViewRotation * m_viewPositionOffset;
			m_vecTargetAimPosition += quatViewRotation * m_aimPositionOffset;

			// Apply the camera panning.
			vecViewPosition += vecDeltaPan;
			m_vecTargetAimPosition += vecDeltaPan;
			m_cameraGroundPosition += vecDeltaPan;
			LimitTargetRange (m_cameraGroundPosition);

			// Gimbal style rotation after it's moved into it's initial position.
			Quat quatOrbitRotation = quatViewRotation * quatPostTransYP;

			// Perform a collision detection. Note, any collisions will disallow use of interpolated
			// camera movement.
			//CollisionDetection (m_vecTargetAimPosition, vecViewPosition);

#ifdef _DEBUG
			if (m_isDebugAllowed)
			{
				// Initial position for our target.
				gEnv->pRenderer->GetIRenderAuxGeom ()->DrawSphere (m_vecTargetPosition, 0.16f, ColorB (0, 0, 128));

				// Position at which we are aiming.
				gEnv->pRenderer->GetIRenderAuxGeom ()->DrawSphere (m_vecTargetAimPosition, 0.12f, ColorB (255, 0, 0));
			}
#endif

			// Store the last position for later.
			m_vecLastTargetPosition = m_vecTargetPosition;
			m_vecLastTargetAimPosition = m_vecTargetAimPosition;

			// Return our new calculated camera pose.
			m_vecLastPosition = vecViewPosition;
			return CCameraPose (vecViewPosition, quatOrbitRotation);
		}

		// Worst case! We return a view based on the view params that were passed in.
		return CCameraPose (viewParams.position, viewParams.rotation);
	}


	Vec3 RTSCamera::CalculateButtonPanning (float frameTime, const Quat& CameraRotation)
	{
		bool allowPanning = true;
		Quat quatPanning;
		Vec3 vecDeltaPan = Vec3 (0.0f, 0.0f, 0.0f);

		// Take the mask and turn it into a vector to indicate the direction we need to pan independant of the
		// present camera direction.
		switch (ButtonPanningMask)
		{
			case ePanningMask_Forward:
				quatPanning = Quat::CreateIdentity ();
				break;

			case (ePanningMask_Forward | ePanningMask_Right) :
				quatPanning = Quat::CreateRotationZ (DEG2RAD (45.0f));
				break;

			case ePanningMask_Right:
				quatPanning = Quat::CreateRotationZ (DEG2RAD (90.0f));
				break;

			case (ePanningMask_Back | ePanningMask_Right) :
				quatPanning = Quat::CreateRotationZ (DEG2RAD (135.0f));
				break;

			case ePanningMask_Back:
				quatPanning = Quat::CreateRotationZ (DEG2RAD (180.0f));
				break;

			case (ePanningMask_Back | ePanningMask_Left) :
				quatPanning = Quat::CreateRotationZ (DEG2RAD (225.0f));
				break;

			case ePanningMask_Left:
				quatPanning = Quat::CreateRotationZ (DEG2RAD (270.0f));
				break;

			case (ePanningMask_Forward | ePanningMask_Left) :
				quatPanning = Quat::CreateRotationZ (DEG2RAD (315.0f));
				break;

			default:
				quatPanning = Quat::CreateIdentity ();
				allowPanning = false;
				break;
		}

		if (allowPanning)
		{
			// Find out the current FoV.
			float fov = gEnv->pRenderer->GetCamera ().GetFov ();
			float theta = (gf_PI - fov) / 2.0f;

			// Create a vector based on camera direction, key direction, speed and frame time.
			float cameraRange = MAX (m_cameraHeight, (m_targetDistance + m_cameraHeight) * cl_tpvZoom);
			float base = 2.0f * cameraRange / abs (tan (theta));
			float zoomDistance = base * cl_tpvPanKeySpeed;
			float panDistance = zoomDistance * frameTime;
			vecDeltaPan = Vec3 (CameraRotation.GetFwdX (), CameraRotation.GetFwdY (), 0.0f).GetNormalized () * quatPanning * panDistance;
		}

		return vecDeltaPan;
	}


	void RTSCamera::LimitTargetRange (Vec3& vecTarget)
	{
		Limit (vecTarget.x, cl_tpvPanHorizontalMin, cl_tpvPanHorizontalMax);
		Limit (vecTarget.y, cl_tpvPanVerticalMin, cl_tpvPanVerticalMax);
	}


	void RTSCamera::ResetMovements ()
	{
		ICamera::ResetMovements ();
	}
}

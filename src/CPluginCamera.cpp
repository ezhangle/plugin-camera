/* Camera_Plugin - for licensing and copyright see license.txt */

#include <StdAfx.h>
#include <CPluginCamera.h>
#include <ISystem.h>
#include <Chrysalis/Player/Camera/CameraHandler.h>


#define FILTER_ACTION(filter,action) pActionMapManager->GetActionFilter( filter )->Filter( action );

namespace CameraPlugin
{
	CPluginCamera* gPlugin = nullptr;

	CPluginCamera::CPluginCamera ()
	{
		gPlugin = this;
		m_thirdPersonView = new Chrysalis::CCameraHandler ();
	}


	CPluginCamera::~CPluginCamera ()
	{
		delete m_thirdPersonView;
		Release (true);
		gPlugin = nullptr;
	}


	bool CPluginCamera::Init (SSystemGlobalEnvironment& env, SSystemInitParams& startupParams, IPluginBase* pPluginManager,
		const char* sPluginDirectory)
	{
		gPluginManager = (PluginManager::IPluginManager*)pPluginManager->GetConcreteInterface (nullptr);
		CPluginBase::Init (env, startupParams, pPluginManager, sPluginDirectory);

		if (gEnv && gEnv->pSystem && !gEnv->pSystem->IsQuitting ())
		{
			// Plugin specific initialisation happens here.
			m_thirdPersonView->Init ();
		}

		// Note: Autoregister Flownodes will be automatically registered

		return true;
	}


	bool CPluginCamera::Release (bool bForce)
	{
		bool bRet = true;

		if (!m_bCanUnload)
		{
			// Should be called while Game is still active otherwise there might be leaks/problems
			bRet = CPluginBase::Release (bForce);

			if (bRet)
			{
				// Depending on your plugin you might not want to unregister anything
				// if the System is quitting.
				if (gEnv && gEnv->pSystem && !gEnv->pSystem->IsQuitting ())
				{
				}

				// Cleanup like this always (since the class is static its cleaned up when the dll is unloaded)
				gPluginManager->UnloadPlugin (GetName ());

				// Allow Plugin Manager garbage collector to unload this plugin
				AllowDllUnload ();
			}
		}

		return bRet;
	};


	const char* CPluginCamera::ListCVars () const
	{
		return "cl_tpvZoom, cl_tpvZoomMin, cl_tpvZoomMax, cl_tpvZoomStep, cl_tpvViewPitch, cl_tpvViewYaw";
	}


	const char* CPluginCamera::GetStatus () const
	{
		return "OK";
	}
}


// HACK: I'm defining this to get around the external decl from including Player.h
// Don't use this variable, use gEnv instead.
CGame* g_pGame = nullptr;

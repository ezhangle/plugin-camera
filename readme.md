# Camera Plugin for CryEngine SDK #

The Camera Plugin is a flexible third person camera system for CryEngine 3.6.x projects. It aims to replace the very simple third person camera that ships with CryEngine 3.

Initial support is for an Action RPG camera, similar to that found in The Elder Scrolls Online and Tera. Support for other styles of camera, such as sidescrolling or RTS may be added at a later date if there is demand. There is currently a near complete implementation of an RTS camera which incorporates the best features of an Action RPG camera with RTS movements. There was an implementation of a simple RPG style camera, but since it was almost identical to the Action RPG camera it has been removed for now until work on the Action RPG camera is completed.


## Action RPG Camera Implementation ##

The Action RPG camera is capable of being attached to any entity within the game and following that entity at a user configurable distance. It can attach either directly to an entity or to a bone within the entity giving it a fair degree of flexibility.

The Action RPG camera tracks mouse and XBox controller actions and responds by orbiting the camera around the entity. This is all handled using action maps, making it easy to customise which keys allow you to tilt and pan around the player character. Zooming in and out is equally supported.

The camera applies three forms of interpolation to camera movements in order to provide you with the smoothest possible camera experience.

* linear interpolation of zooming in and out
* spherical linear interpolation of yaw and pitch rotations about the locus (anchor entity)
* spherical linear interpolation of changes in the direction the resulting camera is aiming

The zoom feature, which was originally linear is now performed using an exponential function, allowing for small zoom steps in close and large ones the further out it is zoomed.

Outstanding Issues:

* a proper collision detection routine
* player rotation needs to be handled by the end user code
* aim needs to be handled by the player movement code


## Action RTS Camera Implementation ##

This is an almost complete experimental implementation of an Action RTS camera. It combines features from the Action RPG camera and traditional RTS movements. The camera supports:

* attaching to an entity and following it around
* jumping to an entity and staying in place
* jumping to a position
* full 360 degree (user configurable) pitch and yaw with panning relative to the present camera direction
* interpolated zoom
* panning over the map using arrow keys
* ground hugging as the camera pans over the terrain
* a FoV and distance aware panning function that increases panning speed the further from the terrain you zoom out
* interpolation

Needing implementation:

* a proper collision detection routine
* panning from mouse movements beyond the screen edge

Like all experimental code, there are some rough patches and areas where I simply haven't finished the implementation. Most are fairly trivial to complete.

Outstanding Issues:

* height when detaching the camera from an entity
* collision detection


## Installation / Integration ##

See the [wiki](https://bitbucket.org/ivanhawkes/plugin-camera/wiki/) for [installation / integration](https://bitbucket.org/ivanhawkes/plugin-camera/wiki/Installation%20/%20Integration)


## Flownodes ##


### Chrysalis:Camera:Settings ###

This flownode sets up all the various camera variables that are common to the camera implementations. It is here that you will set zoom lengths, minimum and maximum values for pitch and yaw, inverted Y axis, etc.


### Chrysalis:Camera:ActionRPGCamera ###

This flownode sets up and controls an Action RPG style of camera.

In order to use it you will need to take a few steps:

* Set it's entityID - this is needed to know which entity it should orbit. Nothing will work if you haven't set this correctly.
* Fill in all the settings (a list of bones will be available *after* an entity has been set for the flownode).
* Apply the settings by firing the 'Set' input node - this locks in the settings and performs a lookup for an actor / player based on the entity at the time. If you change the entity you are attached to you will need to fire this again.
* Enable the camera by firing the 'Enable' input node. The camera will respond to updates until you fire the 'Disable' node.

You can have several of these flownodes set up and active if you like, but only one can be enabled at any given time for fairly obvious reasons. If none of them are active then we fall back onto the in-built third person camera.

NOTE: If you update any settings in the flownode, they will not take effect until you fire the 'Activate' input node.


### Chrysalis:Camera:RTSCamera ###

Similar to the Action RPG node, this node provides the experimental implementation of the Action RTS camera.

The camera has several ways of being used, and each needs slightly different setup.

To jump to a position, simply set the 'NewPosition' value and activate 'MoveToPosition'.

To attach to an entity, set the EntityID of the node first, then activate 'AttachToEntity'. Likewise, to just jump to the entity but remain unattached, set the EntityID of the node, then activate 'MoveToEntity'.


## Known Issues ##

The code is still pre-alpha and has some known issues.

* The player character shouldn't turn when we are panning the camera, but this is not yet implemented. This needs to be handled by the player input and movement code. Work is underway to find out where and how to do this.
* Aiming is also meant to be handled by the player input / movement code. Trying to run around and aim is currently an exercise in pain and frustration.
* The collision detection is purely awful and is only there as a placeholder.


## Planned Improvements ##

TBA